using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaAttackAnimation : MonoBehaviour
{
    float attackCount;
    [SerializeField]
    float frameTime;
    [SerializeField]
    float frame;

    [SerializeField]
    bool music_TooNear;
    [SerializeField]
    GameObject music;
    void OnEnable()
    {
        if (music_TooNear == true)
        {
            Instantiate(music);
        }
    }

    // Update is called once per frame
    void Update()
    {
        attackCount += Time.deltaTime;
        if (attackCount >= (frame - 1) * frameTime) 
        {
            gameObject.SetActive(false);
            attackCount = 0;
        }
    }
}
