using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealCtrl : MonoBehaviour
{
    [SerializeField]
    bool isSingle;
    [SerializeField]
    float needCrystal;
    [SerializeField]
    GameObject m_Attack;
    [SerializeField]
    GameObject s_Attack;
    [SerializeField]
    GameObject m_AttackColi;
    public void Heal()
    {
        if (isSingle == false)
        {
            m_Attack.SetActive(true);
            if (WorldControler.crystal >= needCrystal)
            {
                WorldControler.crystal -= needCrystal;
                s_Attack.SetActive(true);
            }
        }
        if (isSingle == true)
        {
            if (WorldControler.crystal >= needCrystal)
            {
                WorldControler.crystal -= needCrystal;

                GameObject bullet = Instantiate(m_Attack, gameObject.transform.position, m_Attack.transform.rotation);
                bullet.GetComponent<SingleAttackCtrl>().SetTarget(m_AttackColi.GetComponent<TowerAttack>().healTarget.transform);

                s_Attack.SetActive(true);
            }
        }
    }
}
