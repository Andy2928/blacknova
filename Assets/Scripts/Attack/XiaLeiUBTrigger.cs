using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XiaLeiUBTrigger : MonoBehaviour
{
    [SerializeField]
    GameObject m_UBAtk;
    [SerializeField]
    Animator a_XiaLei;
    public void UB()
    {
        m_UBAtk.SetActive(true);
    }
    public void closeUB()
    {
        a_XiaLei.SetBool("UB", false);
    }
}
