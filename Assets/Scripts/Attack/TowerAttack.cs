using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerAttack : MonoBehaviour
{
    [SerializeField]
    Animator a_Tower;
    public List<GameObject> enemys = new List<GameObject>();//enemys = �ؼ�
    public GameObject healTarget;

    [SerializeField]
    GameObject s_Attack;

    [Header("1�Q���L�B�q�ϧL�B50�����L")]
    [SerializeField]
    int towerType;

    [Header("�]�k�L��")]
    [SerializeField]
    bool isMagic;
    [SerializeField]
    float needCrystal;

private void Update()
    {
        Attack();
    }

    void Attack()
    {
        if (towerType == 50)
        {
            if (healTarget != null)
            {
                for (int i = 0; i < enemys.Count; i++)
                {
                    if (enemys[i].GetComponent<TowerSetting>().hp < enemys[i].GetComponent<TowerSetting>().maxHp && WorldControler.crystal >= needCrystal)
                    {
                        Debug.Log(enemys[i].name);
                        healTarget = enemys[i];
                        a_Tower.SetBool("Attack", true);
                    }
                }

                if (healTarget.GetComponent<TowerSetting>().hp >= healTarget.GetComponent<TowerSetting>().maxHp || WorldControler.crystal < needCrystal) 
                {
                    a_Tower.SetBool("Attack", false);
                }
            }
        }
        else
        {
            if (enemys.Count == 0 || isMagic == true && WorldControler.crystal < needCrystal)
            {
                a_Tower.SetBool("Attack", false);

                if (towerType == 1)
                {
                    s_Attack.SetActive(false);
                }
            }

            if (enemys.Count > 0)
            {
                if (isMagic == false || isMagic == true && WorldControler.crystal >= needCrystal)
                {
                    if (enemys[0] == null)
                    {
                        enemys.Remove(enemys[0]);
                    }

                    a_Tower.SetBool("Attack", true);

                    if (towerType == 1)
                    {
                        s_Attack.SetActive(true);
                    }
                }
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Enemy")
        {
            enemys.Add(other.gameObject);
        }
        if(other.tag == "Tower" && towerType == 50)
        {
            healTarget = other.gameObject;
            enemys.Add(other.gameObject);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Enemy")
        {
            enemys.Remove(other.gameObject);
        }
    }
}
