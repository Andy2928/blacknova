using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomAttack : MonoBehaviour
{
    [SerializeField]
    GameObject m_Attack;
    //[SerializeField]
    //Transform m_Target;
    [SerializeField]
    TowerAttack m_TATarget;
    public void Attack()
    {
        GameObject bullet = Instantiate(m_Attack, transform.parent.position, transform.rotation);
        bullet.GetComponent<BoomBullet>().SetTarget(m_TATarget.enemys[0].transform);
    }
}
