using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomBullet : MonoBehaviour
{
    [SerializeField]
    float speed;
    [SerializeField]
    float aniTime;
    [SerializeField]
    GameObject hurtColi;

    Transform target;

    [SerializeField]
    GameObject atkTarget;

    public void SetTarget(Transform _target)
    {
        target = _target;
    }
    private void Start()
    {
        Destroy(gameObject, aniTime);
        atkTarget.transform.position = target.position;//設定子彈落點
        atkTarget.transform.parent = null;//讓子彈落點和子彈分離
    }
    // Update is called once per frame
    void Update()
    {
        if (atkTarget != null)
        {
            transform.position = Vector3.MoveTowards(gameObject.transform.position, atkTarget.transform.position, speed * Time.deltaTime);
            float tmp = Vector3.Distance(transform.position, atkTarget.transform.position);
            if (tmp <= 0.02f)
            {
                hurtColi.SetActive(true);
                Destroy(atkTarget);
            }
        }
    }
}
