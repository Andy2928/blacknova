using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SafenteAtkColor : MonoBehaviour
{
    [SerializeField]
    bool isUbOn;
    [SerializeField]
    GameObject safenteHeal;
    [SerializeField]
    GameObject safenteSpeed;
    public int buffType;
    [SerializeField]
    Image ubBtn;
    [SerializeField]
    Sprite ubHeal;
    [SerializeField]
    Sprite ubSpeed;

    float timeCount;
    public void UB()
    {
        isUbOn = !isUbOn;
    }
    private void Update()
    {
        if(WorldControler.crystal < 15)
        {
            isUbOn = false;
        }

        if (!isUbOn)
        {
            ubBtn.sprite = ubHeal;
            buffType = 0;
            safenteHeal.SetActive(true);
            safenteSpeed.SetActive(false);
        }
        if (isUbOn)
        {
            ubBtn.sprite = ubSpeed;
            buffType = 1;
            safenteSpeed.SetActive(true);
            safenteHeal.SetActive(false);

            timeCount += Time.deltaTime;
            if(timeCount >= 1)
            {
                WorldControler.crystal -= 15;
                timeCount = 0;
            }
        }
    }
}
