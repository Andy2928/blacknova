using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackTrigger : MonoBehaviour
{
    [SerializeField]
    bool isElectric;
    [SerializeField]
    bool isSingle;
    [SerializeField]
    bool isMagic;
    [SerializeField]
    float needCrystal;
    [SerializeField]
    GameObject m_Attack;
    [SerializeField]
    GameObject s_Attack;
    [SerializeField]
    GameObject m_AttackColi;
    [Header("�]���S��")]
    [SerializeField]
    bool isMagicBoom;
    [SerializeField]
    TowerBuilding TowerFather;
    [SerializeField]
    GameObject s_Attack_U;
    [SerializeField]
    GameObject s_Attack_D;
    [SerializeField]
    GameObject s_Attack_R;

    public void Attack()
    {
        if (isMagicBoom == true)
        {
            m_Attack.SetActive(true);
            if (WorldControler.crystal >= needCrystal && WorldControler.crystal != 0)
            {
                WorldControler.crystal -= needCrystal;
                if (TowerFather.magicBoomDirec == 1)
                {
                    s_Attack_U.SetActive(true);
                }
                if (TowerFather.magicBoomDirec == 2)
                {
                    s_Attack_D.SetActive(true);
                }
                if (TowerFather.magicBoomDirec == 3)
                {
                    s_Attack_R.SetActive(true);
                }
            }
        }
        else
        {
            if (isSingle == false)
            {
                m_Attack.SetActive(true);
                if (isMagic == true && WorldControler.crystal >= needCrystal && WorldControler.crystal != 0)
                {
                    WorldControler.crystal -= needCrystal;
                    s_Attack.SetActive(true);
                }
                if (isMagic == false && isElectric == false)
                {
                    s_Attack.SetActive(true);
                }
            }
            if (isSingle == true)
            {
                if (isMagic == true && WorldControler.crystal >= needCrystal)
                {
                    WorldControler.crystal -= needCrystal;

                    GameObject bullet = Instantiate(m_Attack, gameObject.transform.position, m_Attack.transform.rotation);
                    bullet.GetComponent<SingleAttackCtrl>().SetTarget(m_AttackColi.GetComponent<TowerAttack>().enemys[0].transform);

                    s_Attack.SetActive(true);
                }

                if (isMagic == false)
                {
                    GameObject bullet = Instantiate(m_Attack, gameObject.transform.position, m_Attack.transform.rotation);
                    bullet.GetComponent<SingleAttackCtrl>().SetTarget(m_AttackColi.GetComponent<TowerAttack>().enemys[0].transform);

                    s_Attack.SetActive(true);
                }
            }
        }
    }
}
