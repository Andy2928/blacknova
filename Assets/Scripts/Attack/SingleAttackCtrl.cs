using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleAttackCtrl : MonoBehaviour
{
    [SerializeField]
    bool isHealer;
    [SerializeField]
    bool isMagic;
    [SerializeField]
    GameObject bulletEffect;
    [SerializeField]
    [Header("0��A1�ĤH")]
    float attackType;
    [SerializeField]
    float dmg;
    [SerializeField]
    float speed;
    [SerializeField]
    GameObject m_Skin;
    Transform target;
    float localScale_x;
    // Update is called once per frame

    [SerializeField]
    bool music_TooNear;
    [SerializeField]
    GameObject music;
    private void Start()
    {
        if(music_TooNear == true)
        {
            Instantiate(music);
        }
        localScale_x = m_Skin.transform.localScale.x;
    }
    void Update()
    {
        BulletMove();
    }

    public void SetTarget(Transform _target)
    {
        target = _target;
        //Debug.Log(target);
    }
    void BulletMove()
    {
        if (attackType == 0)
        {
            if (target == null)
            {
                Destroy(gameObject);
            }
            if (target != null)
            {
                transform.position = Vector3.MoveTowards(gameObject.transform.position, target.transform.position, speed * Time.deltaTime);
                float tmp = Vector3.Distance(transform.position, target.transform.position);
                if (tmp <= 0.02f)
                {
                    if (isHealer == true)
                    {
                        target.gameObject.GetComponent<TowerSetting>().hp += dmg;
                    }
                    else
                    {
                        if (isMagic == true)
                        {
                            target.gameObject.GetComponent<EnemyHp>().hp -= dmg;
                        }
                        else if ((dmg - target.gameObject.GetComponent<EnemyHp>().armorValue) > 0)
                        {
                            target.gameObject.GetComponent<EnemyHp>().hp -= (dmg - target.gameObject.GetComponent<EnemyHp>().armorValue);
                        }
                        else
                        { }

                        if (bulletEffect != null)
                        {
                            Instantiate(bulletEffect, transform.position, bulletEffect.transform.rotation);
                        }
                        if (target.gameObject.GetComponent<EnemyHp>().attacked_Effect != null)
                        {
                            Instantiate(target.gameObject.GetComponent<EnemyHp>().attacked_Effect, transform.position, target.gameObject.GetComponent<EnemyHp>().attacked_Effect.transform.rotation);
                        }
                        target.gameObject.GetComponent<EnemyHp>().b_OnAttacked = false;
                        target.gameObject.GetComponent<EnemyHp>().b_OnAttacked = true;
                    }
                    Destroy(gameObject);
                }
            }
        }
        if (attackType == 1)
        {
            if (target == null)
            {
                Destroy(gameObject);
            }
            if (target != null)
            {
                transform.position = Vector3.MoveTowards(gameObject.transform.position, target.transform.position, speed * Time.deltaTime);
                if (target.transform.position.x - transform.position.x > .2f)
                {
                    m_Skin.transform.localScale = new Vector3(localScale_x, m_Skin.transform.localScale.y, m_Skin.transform.localScale.z);
                }
                if (target.transform.position.x - transform.position.x < -.2f)
                {
                    m_Skin.transform.localScale = new Vector3(-localScale_x, m_Skin.transform.localScale.y, m_Skin.transform.localScale.z);
                }
                float tmp = Vector3.Distance(transform.position, target.transform.position);
                if (tmp <= 0.02f)
                {
                    if (target.tag == "Castle")
                        target.gameObject.GetComponent<CastleControler>().hp -= dmg;
                    if (target.tag == "Tower")
                    {
                        if (DifficultyCtrl.difficulty == 0)
                        {
                            target.gameObject.GetComponent<TowerSetting>().hp -= dmg * DifficultyCtrl.eazySet;
                        }
                        if (DifficultyCtrl.difficulty == 1)
                        {
                            target.gameObject.GetComponent<TowerSetting>().hp -= dmg;
                        }

                        if (target.gameObject.GetComponent<TowerSetting>().attacked_Effect != null)
                        {
                            Instantiate(target.gameObject.GetComponent<TowerSetting>().attacked_Effect, transform.position, target.gameObject.GetComponent<TowerSetting>().attacked_Effect.transform.rotation);
                        }

                        target.gameObject.GetComponent<TowerSetting>().b_OnAttacked = false;
                        target.gameObject.GetComponent<TowerSetting>().b_OnAttacked = true;
                    }

                    if (bulletEffect != null)
                    {
                        Instantiate(bulletEffect, transform.position, bulletEffect.transform.rotation);
                    }
                    Destroy(gameObject);
                }
            }
        }
    }
}
