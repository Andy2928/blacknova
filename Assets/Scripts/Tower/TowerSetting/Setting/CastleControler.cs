using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CastleControler : MonoBehaviour
{
    [SerializeField]
    GameObject buildTowerUI;
    public float hp = 30;
    [SerializeField]
    float maxHp = 30;

    [SerializeField]
    GameObject towerPicture;
    [SerializeField]
    GameObject ui_HpSlider;
    [SerializeField]
    Image hpSlider;
    [SerializeField]
    Image pictureHpSlider;
    [SerializeField]
    GameObject selectFrame;
    [SerializeField]
    GameObject selectedFrame;
    [SerializeField]
    GameObject m_DeadIdol;


    public float buildCd;
    public GameObject[] btns;


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        HPSliderCtrl();
    }

    void HPSliderCtrl()
    {
        hpSlider.fillAmount = hp / maxHp;
        pictureHpSlider.fillAmount = hp / maxHp;


        if (hpSlider.fillAmount < 1)
        {
            ui_HpSlider.SetActive(true);
        }
        else
        {
            ui_HpSlider.SetActive(false);
        }

        if (hp <= 0)
        {
            Instantiate(m_DeadIdol, gameObject.transform.position, m_DeadIdol.transform.rotation);
            Destroy(gameObject);
        }
    }

    public void OnMouseDown()
    {
        if (WorldControler.buildType == 0 && !EventSystem.current.IsPointerOverGameObject())
        {
            GameObject[] UIs = GameObject.FindGameObjectsWithTag("UI");
            for (int i = 0; i < UIs.Length; i++)
            {
                UIs[i].SetActive(false);
            }

            towerPicture.SetActive(true);
            buildTowerUI.SetActive(true);
            WorldControler.nowBuildBarrack = gameObject;
            selectedFrame.SetActive(true);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "EBoomAtk")
        {
            hp -= 300;
        }
    }
    private void OnMouseOver()
    {
        if (!EventSystem.current.IsPointerOverGameObject() && WorldControler.buildType == 0)
        {
            selectFrame.SetActive(true);
        }
    }
    private void OnMouseExit()
    {
        selectFrame.SetActive(false);
    }
}
