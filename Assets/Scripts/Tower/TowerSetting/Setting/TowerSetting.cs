using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TowerSetting : MonoBehaviour
{
    [Header("父物件")]
    [SerializeField]
    GameObject m_Parent;
    [SerializeField]
    SpriteRenderer tower_Skin;
    [Header("菁英代碼")]
    [SerializeField]
    int eliteCode;

    public bool buildSet;

    [SerializeField]
    [Header("特殊塔編號:0其他，1宿舍，2資源點")]
    float specialTower;
    public float hp;
    public float maxHp;

    [Header("受擊")]
    public bool b_OnAttacked;
    float onAttackedTimeCount;
    public GameObject attacked_Effect;

    [Header("死亡")]
    public GameObject m_DeadIdol;

    [Header("菁英即時對話(離場)")]
    [SerializeField]
    Text eliteOut;

    [Header("其他")]
    [SerializeField]
    float removeMultiple = 0.2f;
    [SerializeField]
    GameObject tower_Depoly;
    [SerializeField]
    GameObject RemoveBtn;
    [SerializeField]
    GameObject towerPicture;
    [SerializeField]
    GameObject selectFrame;
    [SerializeField]
    GameObject selectedFrame;

    [SerializeField]
    GameObject ui_HpSlider;
    [SerializeField]
    Image hpSlider;
    [SerializeField]
    Image pictureHpSlider;
    [SerializeField]
    GameObject TowerRange_UI;

    [Header("萊咪用")]
    [SerializeField]
    Animator a_tower;
    [SerializeField]
    GameObject m_attack;
    [SerializeField]
    float maxHp_Basic;
    [SerializeField]
    GameObject music_LymeiDead;

    [Header("音效")]
    [SerializeField]
    GameObject music_Remove;

    [Header("沙芬特BUFF相關")]
    [SerializeField]
    SafenteAtkColor safenteBuff;
    [SerializeField]
    float safenteHealTimeCount;

    //在不可建造的區域上(陰影中)
    bool inShadow = false;

    private void Start()
    {
        eliteLvUp();
        hp = maxHp;
    }
    void Update()
    {
        HpDebug();
        HPSliderCtrl();
        OnAttacked();
        DeadCtrl();
        SafenteBuff();

        //萊咪特製
        if (buildSet == false && eliteCode == 2 && a_tower != null)
        {
            a_tower.speed = 0;
        }
        if (buildSet == true && eliteCode == 2 && a_tower != null)
        {
            a_tower.speed = 1;
        }
        //沙芬特特製
        if (buildSet == true && eliteCode == 3)
        {
            m_attack.SetActive(true);
        }
    }
    void SafenteBuff()
    {
        if(safenteBuff == null || safenteBuff.buffType != 1)
        {
            if (gameObject.GetComponent<Animator>() != null)
                gameObject.GetComponent<Animator>().speed = 1f;
        }
        if (safenteBuff != null)
        {
            if (safenteBuff.buffType == 0 && hp < maxHp)
            {
                safenteHealTimeCount += Time.deltaTime;
                if (safenteHealTimeCount >= 2)
                {
                    hp += 5;
                    safenteHealTimeCount = 0;
                }
            }
            if (safenteBuff.buffType == 1)
            {
                if (gameObject.GetComponent<Animator>() != null)
                    gameObject.GetComponent<Animator>().speed = 1.2f;
            }
        }
    }
    void HpDebug()
    {
        if (hp > maxHp) 
        {
            hp = maxHp;
        }
    }
    void eliteLvUp()
    {
        if (eliteCode == 2)
        {
            if (WorldControler.LimeyLV_8 == true)
                maxHp = maxHp_Basic + 450;
            else if (WorldControler.LimeyLV_7 == true)
                maxHp = maxHp_Basic + 300;
            else if (WorldControler.LimeyLV_5 == true)
                maxHp = maxHp_Basic + 200;
            else if (WorldControler.LimeyLV_4 == true)
                maxHp = maxHp_Basic + 100;
            else if (WorldControler.LimeyLV_2 == true)
                maxHp = maxHp_Basic + 50;
        }
    }
    void OnAttacked()
    {
        if(b_OnAttacked == false)
        {
            onAttackedTimeCount = 0;
            tower_Skin.color = Color.white;
        }
        if(b_OnAttacked == true)
        {
            onAttackedTimeCount += Time.deltaTime;
            if (onAttackedTimeCount <= 0.1f)
            {
                tower_Skin.color = Color.red;
            }
            if(onAttackedTimeCount > 0.1f)
            {
                tower_Skin.color = Color.white;
                b_OnAttacked = false;
            }
        }
    }
    void DeadCtrl()
    {
        if (hp <= 0)
        {
            WorldControler.population -= tower_Depoly.GetComponent<TowerReadyDeploy>().usedPopulation;
            if (m_DeadIdol != null && eliteCode != 2)
            {
                //m_DeadIdol.transform.localScale = transform.localScale;
                Instantiate(m_DeadIdol, transform.position, m_DeadIdol.transform.rotation);
            }
            if (eliteCode == 1)
            {
                GameObject i_Dialog = GameObject.Find("Teachs").GetComponent<FindSupport>().needToFind_1;
                i_Dialog.GetComponent<ImmediateDialog>().xialei_Out = true;
                i_Dialog.GetComponent<ImmediateDialog>().eliteTalking = true;
                i_Dialog.SetActive(true);
            }
            if (eliteCode == 2)
            {
                music_LymeiDead.SetActive(true);
                a_tower.SetBool("Stay", false);
                gameObject.SetActive(false);
                m_attack.SetActive(true);
                WorldControler.LimeyExp += 1;

                //GameObject i_Dialog = GameObject.Find("Teachs").GetComponent<FindSupport>().needToFind_1;
                //i_Dialog.GetComponent<ImmediateDialog>().limey_Out = true;
                //i_Dialog.GetComponent<ImmediateDialog>().eliteTalking = true;
                //i_Dialog.SetActive(true);
            }
            if (eliteCode != 2)
            {
                Destroy(m_Parent);
            }
        }
    }
    void HPSliderCtrl()
    {
        if (eliteCode == 1)
        {
            WorldControler.save_XiaLei = true;
        }
        if (eliteCode == 2)
        {
            WorldControler.save_Limey = true;
        }

        hpSlider.fillAmount = hp / maxHp;
        pictureHpSlider.fillAmount = hp / maxHp;


        if (hpSlider.fillAmount < 1)
        {
            ui_HpSlider.SetActive(true);
        }
        else
        {
            ui_HpSlider.SetActive(false);
        }
    }

    //選取框
    private void OnMouseOver()
    {
        if (specialTower != 2)
        {
            if (buildSet == true && inShadow == false && WorldControler.buildType == 0 && !EventSystem.current.IsPointerOverGameObject())
            {
                selectFrame.SetActive(true);
            }
        }
        if (specialTower == 2)
        {
            if (gameObject.GetComponent<SourcePoint>().isCollecting == true && WorldControler.buildType == 0 && !EventSystem.current.IsPointerOverGameObject())
            {
                if (buildSet == true && inShadow == false && WorldControler.buildType == 0)
                {
                    selectFrame.SetActive(true);
                }
            }
        }
    }
    private void OnMouseExit()
    {
        if (buildSet == true && inShadow == false && WorldControler.buildType == 0)
        {
            selectFrame.SetActive(false);
        }
    }
    public void OnMouseDown()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            if (specialTower != 2)
            {
                if (buildSet == true && inShadow == false && WorldControler.buildType == 0 && !EventSystem.current.IsPointerOverGameObject())
                {
                    GameObject[] UIs = GameObject.FindGameObjectsWithTag("UI");
                    for (int i = 0; i < UIs.Length; i++)
                    {
                        UIs[i].SetActive(false);
                    }

                    TowerRange_UI.SetActive(true);
                    towerPicture.SetActive(true);
                    RemoveBtn.SetActive(true);
                    selectedFrame.SetActive(true);
                }
            }
            if (specialTower == 2)
            {
                if (gameObject.GetComponent<SourcePoint>().isCollecting == true && !EventSystem.current.IsPointerOverGameObject())
                {
                    GameObject[] UIs = GameObject.FindGameObjectsWithTag("UI");
                    for (int i = 0; i < UIs.Length; i++)
                    {
                        UIs[i].SetActive(false);
                    }

                    TowerRange_UI.SetActive(true);
                    towerPicture.SetActive(true);
                    RemoveBtn.SetActive(true);
                    selectedFrame.SetActive(true);
                }
            }
        }
    }
    public void RemoveTower()
    {
        Instantiate(music_Remove);
        if (specialTower == 0)
        {
            WorldControler.crystal += tower_Depoly.GetComponent<TowerReadyDeploy>().usedCrystal * removeMultiple;
            WorldControler.gas += tower_Depoly.GetComponent<TowerReadyDeploy>().usedGas * removeMultiple;
            WorldControler.food += tower_Depoly.GetComponent<TowerReadyDeploy>().usedFood * removeMultiple;

            WorldControler.population -= tower_Depoly.GetComponent<TowerReadyDeploy>().usedPopulation;
        }
        if (specialTower == 1)
        {
            WorldControler.crystal += tower_Depoly.GetComponent<TowerReadyDeploy>().usedCrystal * removeMultiple;
            WorldControler.gas += tower_Depoly.GetComponent<TowerReadyDeploy>().usedGas * removeMultiple;
            WorldControler.food += tower_Depoly.GetComponent<TowerReadyDeploy>().usedFood * removeMultiple;

            WorldControler.maxPopulation -= 5;
        }

        if (eliteCode == 1)
        {
            GameObject i_Dialog = GameObject.Find("Teachs").GetComponent<FindSupport>().needToFind_1;
            i_Dialog.GetComponent<ImmediateDialog>().xialei_Out = true;
            i_Dialog.GetComponent<ImmediateDialog>().eliteTalking = true;
            i_Dialog.SetActive(true);
        }

        if (eliteCode == 2)
        {
            GameObject i_Dialog = GameObject.Find("Teachs").GetComponent<FindSupport>().needToFind_1;
            i_Dialog.GetComponent<ImmediateDialog>().limey_Out = true;
            i_Dialog.GetComponent<ImmediateDialog>().eliteTalking = true;
            i_Dialog.SetActive(true);
        }
        Destroy(transform.parent.gameObject);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Grid")
        {
            if (other.GetComponent<Grid>().isShadow == true)
            {
                inShadow = true;
            }
            if (other.GetComponent<Grid>().isShadow == false)
            {
                inShadow = false;
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "EBoomAtk")
        {
            b_OnAttacked = false;
            b_OnAttacked = true;
            if (attacked_Effect != null)
            {
                Instantiate(attacked_Effect, transform.position, attacked_Effect.transform.rotation);
            }

            if (DifficultyCtrl.difficulty == 0)
            {
                hp -= 300 * DifficultyCtrl.eazySet;
            }
            if(DifficultyCtrl.difficulty == 1)
            {
                hp -= 300;
            }
        }

        if(other.tag == "SafenteBuff")
        {
            safenteBuff = other.gameObject.GetComponent<SafenteAtkColor>();
        }
    }
}
