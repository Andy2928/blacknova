using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildBtn : MonoBehaviour
{
    [Header("建築編號(參照文件<建築編號>)")]
    [SerializeField]
    int buildNumber;

    [Header("菁英代碼")]
    [SerializeField]
    int eliteCode;

    [Header("歸屬")]
    [SerializeField]
    GameObject m_belong;
    [Header("需要晶石數")]
    [SerializeField]
    float needCrystal;
    [Header("需要瓦斯數")]
    [SerializeField]
    float needGas;
    [Header("需要食物數")]
    [SerializeField]
    float needFood;
    [Header("需要人口數")]
    [SerializeField]
    float needPopulation;
    [Header("塔")]
    [SerializeField]
    GameObject Tower;
    [Header("是否是建築")]
    [SerializeField]
    bool b_isBuild;

    public Button buildBtn;
    [SerializeField]
    GameObject t_Note;
    //[SerializeField]
    //GameObject t_NotEnough;

    ColorBlock cb_NotEnough;
    ColorBlock cb_Enough;
    void Start()
    {
        buildBtn.onClick.AddListener(OnClick);

        //按鈕顏色設定
        cb_Enough = buildBtn.colors;

        cb_NotEnough.normalColor = new Color32(200, 200, 200, 128);
        cb_NotEnough.highlightedColor = new Color32(200, 200, 200, 128);
        cb_NotEnough.pressedColor = new Color32(200, 200, 200, 128);
        cb_NotEnough.selectedColor = new Color32(200, 200, 200, 128);
        cb_NotEnough.disabledColor = new Color32(200, 200, 200, 128);
        cb_NotEnough.colorMultiplier = 1;
        cb_NotEnough.fadeDuration = 0.1f;
    }
    private void Update()
    {
        TowerBuildCD();

        //解鎖設定
        if (buildNumber == 0 ||
            buildNumber == 1 && WorldControler.houseBuildTeach == true || //宿舍
            buildNumber == 2 && WorldControler.outpostBuildTeach == true ||//哨塔
            buildNumber == 3 && WorldControler.defenseTeach == true ||//輕工場
            //buildNumber == 4 && WorldControler.productionBuildUnlock == true ||//生產建築
            //buildNumber == 5 && WorldControler.productionBuildUnlock == true ||//生產建築
            //buildNumber == 6 && WorldControler.productionBuildUnlock == true ||//生產建築
            buildNumber == 7 && WorldControler.teachPass_One == true ||//研究中心
            eliteCode == 1 && WorldControler.eliteTeach == true ||//夏蕾
            buildNumber == 9 && WorldControler.pass_Two == true //魔法學院
            )
        {
            buildBtn.gameObject.SetActive(true);
            TowerBuildMod();
        }

        else
        {
            buildBtn.gameObject.SetActive(false);
        }
    }

    void TowerBuildMod()
    {
        //顯示說明欄
        bool mouseOnBtn = RectTransformUtility.RectangleContainsScreenPoint(buildBtn.image.rectTransform, Input.mousePosition);
        if (mouseOnBtn == true)
        {
            t_Note.SetActive(true);
        }
        else
        {
            t_Note.SetActive(false);
        }
        //菁英唯一存在
        if (eliteCode == 1 && WorldControler.save_XiaLei == true ||
            eliteCode == 2 && WorldControler.save_Limey == true)
        {
            buildBtn.interactable = false;
        }
        else if (eliteCode == 1 && WorldControler.save_XiaLei == false && buildBtn.image.fillAmount >= 1 && WorldControler.eliteTeach == true ||
            eliteCode == 2 && WorldControler.save_Limey == false && buildBtn.image.fillAmount >= 1)
        {
            buildBtn.interactable = true;
        }
        //檢測資源夠不夠
        else if (b_isBuild == false)
        {
            if (WorldControler.crystal >= needCrystal && WorldControler.gas >= needGas && WorldControler.food >= needFood && WorldControler.maxPopulation - WorldControler.population >= needPopulation)
            {
                buildBtn.interactable = true;
                buildBtn.colors = cb_Enough;
            }
            else
            {
                buildBtn.colors = cb_NotEnough;
            }
        }
        else if (b_isBuild == true)
        {
            if (WorldControler.crystal >= needCrystal && WorldControler.gas >= needGas && WorldControler.food >= needFood)
            {
                buildBtn.colors = cb_Enough;
            }
            else
            {
                buildBtn.colors = cb_NotEnough;
            }
        }


        if (eliteCode == 0 && buildBtn.image.fillAmount >= 1)
        {
            buildBtn.interactable = true;
        }
    }
    void TowerBuildCD()
    {

        //檢測是不是在冷卻
        if (buildBtn.image.fillAmount < 1)
        {
            buildBtn.interactable = false;
            if (m_belong.tag == "Castle")
            {
                buildBtn.image.fillAmount += 1 / m_belong.GetComponent<CastleControler>().buildCd * Time.deltaTime;
            }
            if (m_belong.tag == "Tower")
            {
                buildBtn.image.fillAmount += 1 / m_belong.GetComponent<BarrackSetting>().buildCd * Time.deltaTime;
            }
        }
    }
    public void OnClick()
    {
        if (b_isBuild == false)
        {
            if (WorldControler.crystal >= needCrystal && WorldControler.gas >= needGas && WorldControler.food >= needFood && WorldControler.maxPopulation - WorldControler.population >= needPopulation)
            {
                if (WorldControler.buildType == 0)
                {
                    WorldControler.crystal -= needCrystal;
                    WorldControler.gas -= needGas;
                    WorldControler.food -= needFood;
                    WorldControler.population += needPopulation;
                    WorldControler.buildType = 1;
                    Instantiate(Tower, transform.position, new Quaternion(0, 0, 0, 0));
                    CloseUI();
                }
            }

            if (WorldControler.crystal < needCrystal)
            {
                var crystalPoint = GameObject.FindGameObjectsWithTag("S_Crystal");
                for (int i = 0; i < crystalPoint.Length; i++)
                {
                    Debug.Log(crystalPoint[i].name);
                    crystalPoint[i].GetComponent<SpriteGlow.SpriteGlowEffect>().EnableInstancing = false;
                }
            }
            if (WorldControler.gas < needGas)
            {
                var gasPoint = GameObject.FindGameObjectsWithTag("S_Gas");
                for (int i = 0; i < gasPoint.Length; i++)
                {
                    Debug.Log(gasPoint[i].name);
                    gasPoint[i].GetComponent<SpriteGlow.SpriteGlowEffect>().EnableInstancing = false;
                }
            }
            if (WorldControler.food < needFood)
            {
                var foodPoint = GameObject.FindGameObjectsWithTag("S_Food");
                for (int i = 0; i < foodPoint.Length; i++)
                {
                    Debug.Log(foodPoint[i].name);
                    foodPoint[i].GetComponent<SpriteGlow.SpriteGlowEffect>().EnableInstancing = false;
                }
            }
            //else
            //{
            //    t_NotEnough.SetActive(true);
            //}
        }
        else if (b_isBuild == true)
        {
            if (WorldControler.crystal >= needCrystal && WorldControler.gas >= needGas && WorldControler.food >= needFood)
            {
                if (WorldControler.buildType == 0)
                {
                    WorldControler.crystal -= needCrystal;
                    WorldControler.gas -= needGas;
                    WorldControler.food -= needFood;
                    WorldControler.population += needPopulation;
                    WorldControler.buildType = 1;
                    Instantiate(Tower, transform.position, new Quaternion(0, 0, 0, 0));
                    CloseUI();
                }
            }

            //else
            //{
            //    t_NotEnough.SetActive(true);
            //}
        }
    }

    void CloseUI()
    {
        GameObject[] UIs = GameObject.FindGameObjectsWithTag("UI");
        for (int i = 0; i < UIs.Length; i++)
        {
            UIs[i].SetActive(false);
        }
    }
}
