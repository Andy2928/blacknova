using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonActiveDebug : MonoBehaviour
{
    [SerializeField]
    GameObject[] Buttons;
    void Update()
    {
        for (int i = 0; i < Buttons.Length; i++)
        {
            Buttons[i].SetActive(true);
        }
    }
}
