using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarrackSetting : MonoBehaviour
{
    public float buildCd;
    public GameObject[] btns;

    private void OnMouseDown()
    {
        if (gameObject.GetComponent<TowerSetting>().buildSet == true)
            WorldControler.nowBuildBarrack = gameObject;
    }
}
