
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BuildSourceCheck : MonoBehaviour
{
    [Header("1�L���A2�ܫ}")]
    [SerializeField]
    float eliteCode;
    [Header("1�ɯťؼСA2�ثe�ƭȡA3�ܫ}��q")]
    [SerializeField]
    float eliteValue;
    [Header("0���ۡA1�׾�A2�˴��A3�H�f")]
    [SerializeField]
    float type;
    [SerializeField]
    float i_Need;
    TMP_Text t_Source;

    private void Start()
    {
        t_Source = GetComponent<TMP_Text>();
    }
    void Update()
    {
        if (eliteCode == 1)
        {
            if (eliteValue == 1)
            {
                if (WorldControler.ShalehLV_2 == false)
                    t_Source.text = "20";
                else if (WorldControler.ShalehLV_3 == false)
                    t_Source.text = "30";
                else if (WorldControler.ShalehLV_4 == false)
                    t_Source.text = "50";
                else if (WorldControler.ShalehLV_5 == false)
                    t_Source.text = "80";
                else if (WorldControler.ShalehLV_6 == false)
                    t_Source.text = "120";
                else if (WorldControler.ShalehLV_7 == false)
                    t_Source.text = "170";
                else if (WorldControler.ShalehLV_8 == false)
                    t_Source.text = "230";
                else if (WorldControler.ShalehLV_9 == false)
                    t_Source.text = "300";
            }
            if (eliteValue == 2)
            {
                t_Source.text = WorldControler.ShalehExp.ToString();
            }
        }
        if (eliteCode == 2)
        {
            if (eliteValue == 1)
            {
                if (WorldControler.LimeyLV_2 == false)
                    t_Source.text = "2";
                else if (WorldControler.LimeyLV_3 == false)
                    t_Source.text = "4";
                else if (WorldControler.LimeyLV_4 == false)
                    t_Source.text = "6";
                else if (WorldControler.LimeyLV_5 == false)
                    t_Source.text = "8";
                else if (WorldControler.LimeyLV_6 == false)
                    t_Source.text = "11";
                else if (WorldControler.LimeyLV_7 == false)
                    t_Source.text = "14";
                else if (WorldControler.LimeyLV_8 == false)
                    t_Source.text = "17";
                else if (WorldControler.LimeyLV_9 == false)
                    t_Source.text = "21";
            }
            if (eliteValue == 2)
            {
                t_Source.text = WorldControler.LimeyExp.ToString();
            }
            if (eliteValue == 3)
            {
                if (WorldControler.LimeyLV_8 == true)
                    t_Source.text = "700";
                else if (WorldControler.LimeyLV_7 == true)
                    t_Source.text = "550";
                else if (WorldControler.LimeyLV_5 == true)
                    t_Source.text = "450";
                else if (WorldControler.LimeyLV_4 == true)
                    t_Source.text = "350";
                else if (WorldControler.LimeyLV_2 == true)
                    t_Source.text = "300";
                else
                    t_Source.text = "250";
            }
        }

        if (eliteValue == 0)
        {
            t_Source.text = i_Need.ToString();
            if (type == 0)
            {
                if (WorldControler.crystal < i_Need)
                {
                    t_Source.color = Color.red;
                }
                else
                {
                    t_Source.color = Color.white;
                }
            }
            if (type == 1)
            {
                if (WorldControler.food < i_Need)
                {
                    t_Source.color = Color.red;
                }
                else
                {
                    t_Source.color = Color.white;
                }
            }
            if (type == 2)
            {
                if (WorldControler.gas < i_Need)
                {
                    t_Source.color = Color.red;
                }
                else
                {
                    t_Source.color = Color.white;
                }
            }
            if (type == 3)
            {
                if (WorldControler.maxPopulation - WorldControler.population < i_Need)
                {
                    t_Source.color = Color.red;
                }
                else
                {
                    t_Source.color = Color.white;
                }
            }
        }
    }
}
