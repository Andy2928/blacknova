using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EliteLevelUpSign : MonoBehaviour
{
    [SerializeField]
    GameObject m_LevelUpSign;
    [SerializeField]
    float eliteCode;
    // Update is called once per frame
    void Update()
    {
        if (eliteCode == 0 && WorldControler.ShalehLevelUp == true)
        {
            m_LevelUpSign.SetActive(true);
            WorldControler.ShalehLevelUp = false;
        }
        if (eliteCode == 1 && WorldControler.LimeyLevelUp == true) 
        {
            m_LevelUpSign.SetActive(true);
            WorldControler.LimeyLevelUp = false;
        }
    }
}
