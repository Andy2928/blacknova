using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FunctionBuildBuilding : MonoBehaviour
{
    bool isBuilding = false;

    [Header("特殊單位1哨塔")]
    [SerializeField]
    int specialTower;
    [Header("七本柱順序")]
    [SerializeField]
    int hotKeyBuild;

    [Header("前期設置")]
    [SerializeField]
    GameObject Tower;
    [SerializeField]
    GameObject TowerSkin;
    [SerializeField]
    GameObject Tower_Depoly;

    [SerializeField]
    GameObject buildIcon;
    [SerializeField]
    Image buildFill;


    [Header("部署時間")]
    [SerializeField]
    float depolyTime;
    [SerializeField]
    GameObject TowerRange_UI;

    [SerializeField]
    GameObject[] sources;

    private void Start()
    {
        buildIcon.SetActive(false);
        buildFill.fillAmount = 0;
    }
    void Update()
    {
        DepolyTower();
    }

    void DepolyTower()
    {
        //放置

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (isBuilding == false)
            {
                if (hit.transform.tag == "FunctionBuild")
                {
                    if (Input.GetMouseButtonUp(0))
                    {
                        hit.transform.tag = "Tower";
                        WorldControler.buildType = 0;
                        TowerReadyDeploy.isDepoling = false;

                        buildIcon.SetActive(true);

                        isBuilding = true;

                        WorldControler.nowBuildBarrack.GetComponent<CastleControler>().buildCd = depolyTime;
                        for (int i = 0; i < WorldControler.nowBuildBarrack.GetComponent<CastleControler>().btns.Length; i++)
                        {
                            WorldControler.nowBuildBarrack.GetComponent<CastleControler>().btns[i].GetComponent<BuildBtn>().buildBtn.GetComponent<Image>().fillAmount = 0;
                        }
                    }
                }
            }
        }
        //撤回步驟
        if (Tower.GetComponent<TowerSetting>().buildSet == false && isBuilding == false)
        {
            if (Input.GetMouseButtonUp(1))
            {
                TowerReadyDeploy.isDepoling = false;
                Tower.SetActive(false);
                Tower_Depoly.SetActive(true);
            }
        }

        //建造時間 
        if (buildIcon.gameObject.activeSelf == true)
        {
            buildFill.fillAmount += Time.deltaTime / depolyTime;
        }

        if (buildFill.fillAmount >= 1)
        {
            Object Music_BuildUp_Function = Resources.Load("Music_BuildUp_Function");
            Instantiate(Music_BuildUp_Function);

            TowerSkin.GetComponent<SpriteRenderer>().color = Color.white;
            buildIcon.SetActive(false);

            Tower.GetComponent<TowerSetting>().buildSet = true;
            TowerRange_UI.SetActive(false);

            buildFill.fillAmount = 0;
            WorldControler.buildType = 0;


            if (hotKeyBuild == 2)
            {
                WorldControler.save_Factory = true;
            }
            if (hotKeyBuild == 3)
            {
                WorldControler.save_Tech = true;
            }
            if (hotKeyBuild == 4)
            {
                WorldControler.save_Magic = true;
            }

            if (specialTower == 2)
            {
                sources[0].SetActive(true);
                sources[1].SetActive(true);
                sources[2].SetActive(true);
                sources[3].SetActive(true);
            }
            if (specialTower == 1 && WorldControler.outpostBuiled == false)
            {
                WorldControler.outpostBuiled = true;
                GameObject.Find("Teachs").GetComponent<FindSupport>().needToFind_1.SetActive(true);
            }
        }
    }
}
