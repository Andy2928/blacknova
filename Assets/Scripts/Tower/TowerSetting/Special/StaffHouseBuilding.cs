using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaffHouseBuilding : MonoBehaviour
{
    bool isBuilding = false;

    [Header("前期設置")]
    [SerializeField]
    GameObject Tower;
    [SerializeField]
    GameObject Tower_Depoly;

    [SerializeField]
    GameObject buildIcon;
    [SerializeField]
    Image buildFill;


    [Header("部署時間")]
    [SerializeField]
    float depolyTime;
    [SerializeField]
    GameObject TowerRange_UI;

    private void Start()
    {
        buildIcon.SetActive(false);
        buildFill.fillAmount = 0;
    }

    void Update()
    {
        DepolyTower();
    }

    void DepolyTower()
    {
        //放置

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (isBuilding == false)
            {
                if (hit.transform.tag == "FunctionBuild")
                {
                    if (Input.GetMouseButtonUp(0))
                    {
                        hit.transform.tag = "Tower";
                        WorldControler.buildType = 0;
                        TowerReadyDeploy.isDepoling = false;

                        buildIcon.SetActive(true);

                        isBuilding = true;

                        WorldControler.nowBuildBarrack.GetComponent<CastleControler>().buildCd = depolyTime;
                        for (int i = 0; i < WorldControler.nowBuildBarrack.GetComponent<CastleControler>().btns.Length; i++)
                        {
                            WorldControler.nowBuildBarrack.GetComponent<CastleControler>().btns[i].GetComponent<BuildBtn>().buildBtn.GetComponent<Image>().fillAmount = 0;
                        }
                    }
                }
            }
        }
        //撤回步驟
        if (Tower.GetComponent<TowerSetting>().buildSet == false && isBuilding == false)
        {
            if (Input.GetMouseButtonUp(1))
            {
                TowerReadyDeploy.isDepoling = false;
                Tower.SetActive(false);
                Tower_Depoly.SetActive(true);
            }
        }

        //建造時間 
        if (buildIcon.activeSelf == true)
        {
            buildFill.fillAmount += Time.deltaTime / depolyTime;
        }

        if (buildFill.fillAmount >= 1)
        {
            Object Music_BuildUp_Function = Resources.Load("Music_BuildUp_Function");
            Instantiate(Music_BuildUp_Function);

            WorldControler.maxPopulation += 5;
            buildIcon.SetActive(false);

            Tower.GetComponent<TowerSetting>().buildSet = true;
            TowerRange_UI.SetActive(false);

            buildFill.fillAmount = 0;
            WorldControler.buildType = 0;


            if (WorldControler.houseBuilded == false)
            {
                WorldControler.houseBuilded = true;
                GameObject.Find("Teachs").GetComponent<FindSupport>().needToFind.SetActive(true);
            }
        }
    }
}
