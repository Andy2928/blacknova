using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimeyRevive : MonoBehaviour
{
    [SerializeField]
    GameObject music_heal;
    [SerializeField]
    Animator m_ani;
    public void Revive()
    {
        music_heal.SetActive(true);
        m_ani.SetBool("Stay", true);
    }
}
