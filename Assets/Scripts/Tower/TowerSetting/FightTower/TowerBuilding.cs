using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerBuilding : MonoBehaviour
{
    bool isBuilding = false;

    [Header("前期設置")]
    [Header("七本柱順序")]
    [SerializeField]
    int hotKeyBuild;
    [SerializeField]
    int EliteNum;
    [SerializeField]
    GameObject Tower;
    [SerializeField]
    GameObject Tower_Depoly;
    [Header("近戰拉TowerAttack、遠程拉Bullets的Prefab")]
    [SerializeField]
    GameObject TowerAttack;
    [SerializeField]
    GameObject s_Attack;
    [SerializeField]
    GameObject TowerRange;
    [SerializeField]
    GameObject TowerRange_UI;

    [SerializeField]
    GameObject upButton;
    [SerializeField]
    GameObject downButton;
    [SerializeField]
    GameObject leftButton;
    [SerializeField]
    GameObject rightButton;

    [SerializeField]
    GameObject upFace;
    [SerializeField]
    GameObject downFace;
    [SerializeField]
    GameObject leftFace;
    [SerializeField]
    GameObject rightFace;

    [SerializeField]
    GameObject m_Up;
    [SerializeField]
    GameObject m_Down;
    [SerializeField]
    GameObject m_Left;
    [SerializeField]
    GameObject m_Right;


    [SerializeField]
    GameObject buildIcon;
    [SerializeField]
    Image buildFill;

    [Header("部署時間")]
    [SerializeField]
    float depolyTime;

    [Header("音效")]
    [SerializeField]
    GameObject music_BuildFinish;
    [SerializeField]
    GameObject music_SelectDirection;

    [Header("魔砲兵")]
    [SerializeField]
    bool isMagicBoom;
    [SerializeField]
    public int magicBoomDirec;//1上2下3左右


    // Start is called before the first frame update
    void Start()
    {
        m_Up.SetActive(true);
        m_Down.SetActive(false);
        m_Left.SetActive(false);
        m_Right.SetActive(false);
        upButton.SetActive(true);
        downButton.SetActive(true);
        leftButton.SetActive(true);
        rightButton.SetActive(true);
        buildIcon.gameObject.SetActive(false);
        buildFill.fillAmount = 0;

        upFace.SetActive(false);
        downFace.SetActive(false);
        leftFace.SetActive(false);
        rightFace.SetActive(false);

        TowerRange_UI.SetActive(true);
        TowerRange.SetActive(false);

        Tower.SetActive(false);
        Tower_Depoly.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        DepolyTower();
    }

    void DepolyTower()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        //放置
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform == upButton.transform || hit.transform == downButton.transform || hit.transform == leftButton.transform || hit.transform == rightButton.transform)
            {
                if (hit.transform == upButton.transform)
                {
                    m_Up.SetActive(true);
                    m_Down.SetActive(false);
                    m_Left.SetActive(false);
                    m_Right.SetActive(false);

                    upFace.SetActive(true);
                    downFace.SetActive(false);
                    leftFace.SetActive(false);
                    rightFace.SetActive(false);

                    if (isMagicBoom == false)
                    {
                        s_Attack.transform.rotation = Quaternion.Euler(90, 90, 0);
                    }
                    TowerAttack.transform.rotation = Quaternion.Euler(0, 90, 0);
                    TowerRange.transform.rotation = Quaternion.Euler(0, 90, 0);
                    TowerRange_UI.transform.rotation = Quaternion.Euler(0, 90, 0);

                    if(isMagicBoom == true)
                    {
                        magicBoomDirec = 1;
                    }
                }
                if (hit.transform == downButton.transform)
                {
                    m_Up.SetActive(false);
                    m_Down.SetActive(true);
                    m_Left.SetActive(false);
                    m_Right.SetActive(false);

                    upFace.SetActive(false);
                    downFace.SetActive(true);
                    leftFace.SetActive(false);
                    rightFace.SetActive(false);

                    if (isMagicBoom == false)
                    {
                        s_Attack.transform.rotation = Quaternion.Euler(90, 270, 0);
                    }
                    TowerAttack.transform.rotation = Quaternion.Euler(0, 270, 0);
                    TowerRange.transform.rotation = Quaternion.Euler(0, 270, 0);
                    TowerRange_UI.transform.rotation = Quaternion.Euler(0, 270, 0);

                    if (isMagicBoom == true)
                    {
                        magicBoomDirec = 2;
                    }
                }
                if (hit.transform == leftButton.transform)
                {
                    m_Up.SetActive(false);
                    m_Down.SetActive(false);
                    m_Left.SetActive(true);
                    m_Right.SetActive(false);

                    upFace.SetActive(false);
                    downFace.SetActive(false);
                    leftFace.SetActive(true);
                    rightFace.SetActive(false);

                    if (isMagicBoom == false)
                    {
                        s_Attack.transform.rotation = Quaternion.Euler(90, 0, 0);
                    }
                    TowerAttack.transform.rotation = Quaternion.Euler(0, 0, 0);
                    TowerRange.transform.rotation = Quaternion.Euler(0, 0, 0);
                    TowerRange_UI.transform.rotation = Quaternion.Euler(0, 0, 0);
                    Tower.transform.localScale = new Vector3(40, 40, 1);

                    if (isMagicBoom == true)
                    {
                        magicBoomDirec = 3;
                    }
                }
                if (hit.transform == rightButton.transform)
                {
                    m_Up.SetActive(false);
                    m_Down.SetActive(false);
                    m_Left.SetActive(false);
                    m_Right.SetActive(true);

                    upFace.SetActive(false);
                    downFace.SetActive(false);
                    leftFace.SetActive(false);
                    rightFace.SetActive(true);

                    if (isMagicBoom == false)
                    {
                        s_Attack.transform.rotation = Quaternion.Euler(-90, 180, 0);
                    }
                    TowerAttack.transform.rotation = Quaternion.Euler(0, 180, 0);
                    TowerRange.transform.rotation = Quaternion.Euler(0, 180, 0);
                    TowerRange_UI.transform.rotation = Quaternion.Euler(0, 180, 0);
                    Tower.transform.localScale = new Vector3(-40, 40, 1);

                    if (isMagicBoom == true)
                    {
                        magicBoomDirec = 3;
                    }
                }

                if (Input.GetMouseButtonUp(0))
                {
                    WorldControler.buildType = 0;
                    TowerReadyDeploy.isDepoling = false;

                    m_Up.SetActive(false);
                    m_Down.SetActive(false);
                    m_Left.SetActive(false);
                    m_Right.SetActive(false);
                    upButton.SetActive(false);
                    downButton.SetActive(false);
                    leftButton.SetActive(false);
                    rightButton.SetActive(false);
                    TowerRange_UI.SetActive(false);

                    buildIcon.gameObject.SetActive(true);
                    music_SelectDirection.SetActive(true);

                    isBuilding = true;

                    if (WorldControler.nowBuildBarrack.tag == "Castle")
                    {
                        WorldControler.nowBuildBarrack.GetComponent<CastleControler>().buildCd = depolyTime;

                        for (int i = 0; i < WorldControler.nowBuildBarrack.GetComponent<CastleControler>().btns.Length; i++)
                        {
                            WorldControler.nowBuildBarrack.GetComponent<CastleControler>().btns[i].GetComponent<BuildBtn>().buildBtn.GetComponent<Image>().fillAmount = 0;
                        }
                    }
                    if (WorldControler.nowBuildBarrack.tag == "Tower")
                    {
                        WorldControler.nowBuildBarrack.GetComponent<BarrackSetting>().buildCd = depolyTime;
                        for (int i = 0; i < WorldControler.nowBuildBarrack.GetComponent<BarrackSetting>().btns.Length; i++)
                        {
                            WorldControler.nowBuildBarrack.GetComponent<BarrackSetting>().btns[i].GetComponent<BuildBtn>().buildBtn.GetComponent<Image>().fillAmount = 0;
                        }
                    }
                }
            }
        }

        //撤回步驟
        if (Tower.GetComponent<TowerSetting>().buildSet == false && isBuilding == false)
        {
            if (Input.GetMouseButtonUp(1))
            {
                TowerReadyDeploy.isDepoling = false;
                Tower.SetActive(false);
                Tower_Depoly.SetActive(true);
            }
        }

        //建造時間 
        if (buildIcon.gameObject.activeSelf == true)
        {
            buildFill.fillAmount += Time.deltaTime / depolyTime;
        }

        if (buildFill.fillAmount >= 1)
        {
            Tower.GetComponent<SpriteRenderer>().color = Color.white;
            buildIcon.SetActive(false);

            Tower.GetComponent<TowerSetting>().buildSet = true;
            TowerRange.SetActive(true);

            music_BuildFinish.SetActive(true);

            if (hotKeyBuild == 5)
            {
                WorldControler.save_XiaLei = true;
            }
            if (hotKeyBuild == 6)
            {
                WorldControler.save_Limey = true;
            }
            if (hotKeyBuild == 7) 
            {
                WorldControler.save_SaFenTe = true;
            }

            //菁英建造完成對話
            //if (EliteNum == 1)
            //{
            //    GameObject i_Dialog = GameObject.Find("Teachs").GetComponent<FindSupport>().needToFind_1;
            //    i_Dialog.GetComponent<ImmediateDialog>().xialei_In = true;
            //    i_Dialog.GetComponent<ImmediateDialog>().eliteTalking = true;
            //    i_Dialog.SetActive(true);
            //}
            //if (EliteNum == 2)
            //{
            //    GameObject i_Dialog = GameObject.Find("Teachs").GetComponent<FindSupport>().needToFind_1;
            //    i_Dialog.GetComponent<ImmediateDialog>().limey_In = true;
            //    i_Dialog.GetComponent<ImmediateDialog>().eliteTalking = true;
            //    i_Dialog.SetActive(true);
            //}
            //if (EliteNum == 3)
            //{
            //    GameObject i_Dialog = GameObject.Find("Teachs").GetComponent<FindSupport>().needToFind_1;
            //    i_Dialog.GetComponent<ImmediateDialog>().safente_Out = true;
            //    i_Dialog.GetComponent<ImmediateDialog>().eliteTalking = true;
            //    i_Dialog.SetActive(true);
            //}

            buildFill.fillAmount = 0;
            WorldControler.buildType = 0;
        }
    }
}
