using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerReadyDeploy : MonoBehaviour
{
    public bool canBuild = false;

    public static bool isDepoling = false;
    [SerializeField]
    [Header("塔碰撞種類:0其他，1工兵")]
    float towerColiderType;
    [SerializeField]
    GameObject tower;

    [SerializeField]
    GameObject direction;
    [SerializeField]
    GameObject direction_Skin;
    [SerializeField]
    GameObject Range_CanBuild;
    [SerializeField]
    GameObject Range_CantBuild;

    //需要消耗資源數
    [Header("花費晶礦數")]
    public float usedCrystal = 0;
    [Header("花費瓦斯數")]
    public float usedGas = 0;
    [Header("花費食物數")]
    public float usedFood = 0;
    [Header("花費人口數")]
    public float usedPopulation = 0;

    // Update is called once per frame
    void Update()
    {
        if (canBuild == false)
        {
            Range_CantBuild.SetActive(true);
            Range_CanBuild.SetActive(false);
        }
        if (canBuild == true)
        {
            Range_CantBuild.SetActive(false);
            Range_CanBuild.SetActive(true);
        }

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit) && hit.transform != gameObject.transform && isDepoling == false)
        {
            if (hit.transform.gameObject.tag == "CanBuild" || hit.transform.gameObject.tag == "CantBuild"
                || hit.transform.gameObject.tag == "Crystal" || hit.transform.gameObject.tag == "Gas"
                || hit.transform.gameObject.tag == "Food")
            {
                gameObject.transform.parent.position = hit.transform.position;

                //確認建造
                if(canBuild == true && Input.GetMouseButtonDown(0))
                {
                    direction.SetActive(true);
                    direction_Skin.SetActive(false);
                    WorldControler.isDeploy = false;
                    isDepoling = true;
                    tower.SetActive(true);
                    gameObject.SetActive(false);

                    Object SetOnGrid_O = Resources.Load("SetOnGrid_O");
                    Instantiate(SetOnGrid_O);
                }
            }
            if (canBuild == false && Input.GetMouseButtonDown(0))
            {
                Object SetOnGrid_X = Resources.Load("SetOnGrid_X");
                Instantiate(SetOnGrid_X);
            }
            if (hit.transform.gameObject.tag == "Tower" || hit.transform.tag == "Worker" || hit.transform.gameObject.tag == "Castle" 
                || hit.transform.gameObject.tag == "Ground" || hit.transform.gameObject.tag == "EnemyIns")
            {
                Vector3 screenpos = Camera.main.WorldToScreenPoint(gameObject.transform.parent.position);//獲取需要移動物體的世界轉螢幕座標

                Vector3 mousepos = Input.mousePosition;

                mousepos.z = screenpos.z - 0.001f;//因為滑鼠只有x，y軸，所以要賦予給滑鼠z軸

                Vector3 worldpos = Camera.main.ScreenToWorldPoint(mousepos);//把滑鼠的螢幕座標轉換成世界座標

                gameObject.transform.parent.position = worldpos;//控制物體移動
            }
        }

        if (Input.GetMouseButtonDown(1))
        {
            direction.SetActive(false);
            direction_Skin.SetActive(true);
            WorldControler.isDeploy = false;
            isDepoling = false;


            WorldControler.crystal += usedCrystal;
            WorldControler.gas += usedGas;
            WorldControler.food += usedFood;
            WorldControler.population -= usedPopulation;

            WorldControler.buildType = 0;

            Destroy(gameObject.transform.parent.gameObject);
        }
        
        if(isDepoling == false)
        {
            Range_CanBuild.transform.rotation = Quaternion.Euler(0, 90, 0);
            direction.SetActive(false);
            direction_Skin.SetActive(true);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        //Debug.Log(other.gameObject.tag);
        if (towerColiderType == 0)
        {
            if (other.tag == "CantBuild" || other.tag == "Crystal" || other.tag == "Gas" || other.tag == "Food" || other.tag == "Tower" || other.tag == "Worker" || other.tag == "Castle" || other.tag == "LimeyRevive")
            {
                canBuild = false;
            }
        }
        if (towerColiderType == 1)
        {
            if (other.tag == "CantBuild" || other.tag == "CanBuild" || other.tag == "Tower" || other.tag == "Worker" || other.tag == "Castle" || other.tag == "LimeyRevive")
            {
                canBuild = false;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        canBuild = true;
    }
}
