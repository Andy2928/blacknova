﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class GridMeshCreate : MonoBehaviour
{
    [Serializable]
    [SerializeField]
    class MeshRange
    {
        public int widght;
        public int height;
    }
    //網格的寬高
    [SerializeField] 
    MeshRange meshRange;

    //生成網格起始點
    [SerializeField] 
    Vector3 startPos;

    //網格生成的父物件
    [SerializeField] 
    Transform parentTran;

    //模板預製體
    [SerializeField] 
    GameObject gridPre;

    private Grid[,] m_grids;

    [SerializeField]
    Grid[,] MeshGridData
    {
        get
        {
            return m_grids;
        }
    }
    //註冊模板事件
    public Action<Grid> gridEvent;


    public void CreateMesh()
    {
        if (meshRange.widght == 0 || meshRange.height == 0)
        {
            return;
        }

        ClearMesh();

        m_grids = new Grid[meshRange.widght, meshRange.height];

        for (int i = 0; i < meshRange.widght; i++)
        {
            for (int j = 0; j < meshRange.height; j++)
            {
                CreateGrid(i, j);
            }
        } 
    }
    /// <summary>
    /// 重载，基于传入宽高数据来创建网格
    /// </summary>
    /// <param name="height"></param>
    /// <param name="widght"></param>
    public void CreateMesh(int height, int widght)
    {
        if (widght == 0 || height == 0)
        {
            return;
        }
        ClearMesh();
        m_grids = new Grid[widght, height];
        for (int i = 0; i < widght; i++)
        {
            for (int j = 0; j < height; j++)
            {
                CreateGrid(i, j);
            }
        } 
    }
    /// <summary>
    /// 根据位置创建一个基本的Grid物体
    /// </summary>
    /// <param name="row">x轴坐标</param>
    /// <param name="column">y轴坐标</param>
    public void CreateGrid(int row, int column)
    {
        GameObject go = Instantiate(gridPre, parentTran);
        Grid grid = go.GetComponent<Grid>();
        
        float posX = startPos.x + grid.gridWidght * row;
        float posZ = startPos.z + grid.girdHeight * column;
        go.transform.position = new Vector3(posX, startPos.y, posZ);
        m_grids[row, column] = grid;
        gridEvent?.Invoke(grid);
    }
    
    /// 刪除地圖
    public void ClearMesh()
    {
        if (m_grids == null || m_grids.Length == 0)
        {
            return;
        }
        foreach (Grid grid in m_grids)
        {
            if (grid.gameObject != null)
            {
                Destroy(grid.gameObject);
            }
        }
        Array.Clear(m_grids, 0, m_grids.Length);
    }
}
