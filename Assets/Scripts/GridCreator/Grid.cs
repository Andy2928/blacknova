﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Grid : MonoBehaviour
{
    GameObject tower;
    GameObject scout;
    public GameObject build;
    public GameObject source;
    public bool isFood = false;
    public bool isCrystal = false;
    public bool isGas = false;
    [SerializeField]
    bool isBlock = false;
    [SerializeField]
    bool isLandgrave = false;
    [SerializeField]
    bool isBuilded = false;
    public bool isShadow = false;
    public float gridWidght;
    public float girdHeight;
    //public Action OnClick;
    [SerializeField]
    GameObject notLandgrave;

    bool b_LightShadow;
    GameObject m_lightScout;

    void Update()
    {
        BuildedCheck();

        if(WorldControler.buildType == 0)
        {
            gameObject.tag = "Grid";
        }
        if (WorldControler.buildType != 0)
        {
            if (isCrystal == true)
            {
                gameObject.tag = "Crystal";
            }
            else if (isGas == true)
            {
                gameObject.tag = "Gas";
            }
            else if (isFood == true)
            {
                gameObject.tag = "Food";
            }
            else if (isLandgrave == true && isBlock == false && isBuilded == false)
            {
                gameObject.tag = "CanBuild";
            }
            else if (isLandgrave == false || isBlock == true || isBuilded == true)
            {
                gameObject.tag = "CantBuild";
            }
        }

        if(isLandgrave == false)
        {
            notLandgrave.SetActive(true);

            if (b_LightShadow == true)
            {
                notLandgrave.transform.GetComponent<SpriteRenderer>().color = new Color32(33, 37, 48, 115);
            }
            else
            {
                notLandgrave.transform.GetComponent<SpriteRenderer>().color = new Color32(33, 37, 48, 200);
            }

            isShadow = true;
        }
        if (isLandgrave == true)
        {
            notLandgrave.SetActive(false);
            isShadow = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "DetectLight")
        {
            m_lightScout = other.gameObject;
            b_LightShadow = true;
        }
        if (other.tag == "Detect")
        {
            scout = other.gameObject;
            isLandgrave = true;
        }
        if (other.tag == "Tower" || other.tag == "Worker" || other.tag == "Castle" || other.tag == "LimeyRevive")
        {
            build = other.gameObject;
            isBuilded = true;
        }
        if (other.tag == "Crystal")
        {
            source = other.gameObject;
            isCrystal = true;
        }
        if (other.tag == "Gas")
        {
            source = other.gameObject;
            isGas = true;
        }
        if (other.tag == "Food")
        {
            source = other.gameObject;
            isFood = true;
        }
    }

    void BuildedCheck()
    {
        if (Input.GetKeyUp(KeyCode.Mouse1))
        {
            build = null;
        }
        if(m_lightScout == null)
        {
            b_LightShadow = false;
        }
        if (scout == null)
        {
            isLandgrave = false;
        }
        if (build == null)
        {
            isBuilded = false;
        }
        if (source == null)
        {
            isCrystal = false;
            isFood = false;
            isGas = false;
        }
    }
}