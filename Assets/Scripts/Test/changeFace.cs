using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeFace : MonoBehaviour
{
    [SerializeField]
    List<RuntimeAnimatorController> ani;
    Animator m_ani;
    // Start is called before the first frame update
    void Start()
    {
        m_ani = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            m_ani.runtimeAnimatorController = ani[0];
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            m_ani.runtimeAnimatorController = ani[1];
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            m_ani.runtimeAnimatorController = ani[2];
        }
    }
}
