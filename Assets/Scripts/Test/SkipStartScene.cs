using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SkipStartScene : MonoBehaviour
{
    [SerializeField]
    GameObject music_Press;
    [SerializeField]
    GameObject DifficultyCtrllor;
    public void StartGame()
    {
        music_Press.SetActive(true);
        DifficultyCtrllor.SetActive(true);
        //SceneManager.LoadScene(1);
        //WorldControler.b_Transition = true;
    }
}
