using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Corners : MonoBehaviour
{
    public static Transform[] corners;
    private void Awake()
    {
        corners = new Transform[transform.childCount];
        for (int i = 0; i < corners.Length; i++) 
        {
            corners[i] = transform.GetChild(i);
        }
    }
}
