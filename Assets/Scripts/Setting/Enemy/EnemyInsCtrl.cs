using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyInsCtrl : MonoBehaviour
{
    //public float nowLevel;
    public bool insFinal;

    public float timeCount;
    [SerializeField]
    float levelTime;

    [Header("?號巢穴")]
    [SerializeField]
    float insNumber;
    GameObject m_InsEnemy;//怪物暫存
    GameObject m_InsArrow;//怪物暫存

    [Header("共有?波")]
    [SerializeField]
    float waves = 5;
    [Header("第?波敵人")]
    [SerializeField]
    GameObject[] Enemys;
    [Header("第?波開始時間")]
    [SerializeField]
    float[] waveStart;
    [Header("第?波結束時間")]
    [SerializeField]
    float[] waveFin;
    [Header("第?波每支兵間隔")]
    [SerializeField]
    float[] insTime;
    [Header("第?波每支兵間隔計時器")]
    [SerializeField]
    float[] insTimeCount;

    //[Header("進度控制器")]
    //[SerializeField]
    //float coreWave;
    //[SerializeField]
    //float[] coreWaveStart;
    [Header("進度條的條")]
    [SerializeField]
    Image ProgressBar_L;
    [Header("整個進度條")]
    [SerializeField]
    GameObject m_ProgressBar;

    [Header("第一次教學")]
    [SerializeField]
    GameObject ImmediateDialog;

    [Header("出怪箭頭設置")]
    [SerializeField]
    GameObject m_Arrow;
    [Header("共有?波箭頭")]
    [SerializeField]
    float arrowWaves;
    [Header("第?波箭頭開始時間")]
    [SerializeField]
    float[] arrowStart;
    [Header("第?波箭頭結束時間")]
    [SerializeField]
    float[] arrowFin;
    [SerializeField]
    float insArrowTimeSet = 5;
    float insArrowTimeCount;
    [SerializeField]
    float insArrowWaveSet = 5;
    float insArrowWaveCount;
    int arrowInsedCount = 0;

    void Start()
    {
        
    }

    void Update()
    {
        if (WorldControler.defenseTeach == true)
        {
            
            m_ProgressBar.SetActive(true);

            if(timeCount >= 5 && WorldControler.isTalk4 == false)
            {
                WorldControler.isTalk4 = true;
                ImmediateDialog.SetActive(true);
            }

            //箭頭
            for (int i = 0; i < arrowWaves; i++)
            {
                if (timeCount >= arrowStart[i] && timeCount <= arrowFin[i])
                {
                    insArrowWaveCount += Time.deltaTime;
                    insArrowTimeCount += Time.deltaTime;
                    if (arrowInsedCount < 5)
                    {
                        if (insArrowTimeCount >= insArrowTimeSet)
                        {
                            m_InsArrow = Instantiate(m_Arrow, transform.position, new Quaternion(0, 0, 0, 0));

                            if (insNumber == 0)
                            {
                                m_InsArrow.GetComponent<ArrowMove>().corners = Corners.corners;
                            }
                            if (insNumber == 1)
                            {
                                m_InsArrow.GetComponent<ArrowMove>().corners = Corners1.corners;
                            }

                            insArrowTimeCount = 0;
                            arrowInsedCount++;
                        }
                    }
                    if (insArrowWaveCount >= insArrowWaveSet)
                    {
                        insArrowWaveCount = 0;
                        arrowInsedCount = 0;
                    }
                }
            }
            if(timeCount >= 80 && WorldControler.isTalk5 == false)
            {
                WorldControler.isTalk5 = true;
                ImmediateDialog.SetActive(true);
            }

            //結束出怪
            if (timeCount >= levelTime)
            {
                insFinal = true;
            }
            //出怪控制
            if (DialogSystem.isTalking == false)
            {
                timeCount += Time.deltaTime;
            }
            for (int i = 0; i < waves; i++)
            {
                if (timeCount >= waveStart[i] && timeCount <= waveFin[i])
                {
                    if (waveStart[i] - timeCount < 10)
                    {
                        ProgressBar_L.gameObject.SetActive(true);
                    }

                    insTimeCount[i] += Time.deltaTime;
                    if (insTimeCount[i] >= insTime[i])
                    {
                        m_InsEnemy = Instantiate(Enemys[i], transform.position, new Quaternion(0, 0, 0, 0));

                        if (insNumber == 0) 
                        { 
                            m_InsEnemy.GetComponent<EnemyMove>().corners = Corners.corners;
                        }
                        if (insNumber == 1)
                        {
                            m_InsEnemy.GetComponent<EnemyMove>().corners = Corners1.corners;
                        }
                        insTimeCount[i] = 0;
                    }
                }
            }
            //進度條
            ProgressBar_L.fillAmount = timeCount / levelTime;

            //進度條閃爍
            //if (coreWave == 2)
            //{
            //    if (timeCount <= coreWaveStart[0] && coreWaveStart[0] - timeCount < 10)
            //    {
            //        ProgressBar.gameObject.SetActive(false);
            //        ProgressBar_L.gameObject.SetActive(true);

            //    }
            //    else if (timeCount <= coreWaveStart[1] && coreWaveStart[1] - timeCount < 10)
            //    {
            //        ProgressBar.gameObject.SetActive(false);
            //        ProgressBar_L.gameObject.SetActive(true);
            //    }
            //    else
            //    {
            //        ProgressBar.gameObject.SetActive(true);
            //        ProgressBar_L.gameObject.SetActive(false);
            //    }
            //}
        }
    }
}
