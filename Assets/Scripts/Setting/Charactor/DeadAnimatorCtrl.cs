using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadAnimatorCtrl : MonoBehaviour
{
    float timeCount;
    [SerializeField]
    float timeSet;

    void Update()
    {
        timeCount += Time.deltaTime;
        if(timeCount >= timeSet)
        {
            Destroy(gameObject);
        }
    }
}
