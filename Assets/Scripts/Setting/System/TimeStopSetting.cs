using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeStopSetting : MonoBehaviour
{   
    [SerializeField]
    int teachLevel;
    [SerializeField]
    bool isGameStop;
    private void OnEnable()
    {
        if(teachLevel == 2 && WorldControler.b_levelTwoEnd == true)
        {
            gameObject.SetActive(false);
            WorldControler.timeStop = false;
        }
        if (teachLevel == 3 && WorldControler.b_levelThreeEnd == true)
        {
            gameObject.SetActive(false);
            WorldControler.timeStop = false;
        }
        if (teachLevel == 4 && WorldControler.b_levelFourEnd == true)
        {
            gameObject.SetActive(false);
            WorldControler.timeStop = false;
        }
        else
        {
            WorldControler.timeStop = true;
        }
    }

    private void Update()
    {
        if (isGameStop == true)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                WorldControler.timeStop = false;
                gameObject.SetActive(false);
            }
        }
    }
    public void OnClick()
    {
        if(isGameStop == true)
        {
            WorldControler.timeStop = false;
            gameObject.SetActive(false);
        }
    }
}
