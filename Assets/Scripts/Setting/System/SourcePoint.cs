using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class SourcePoint : MonoBehaviour
{
    [Header("資源種類,0礦1瓦斯2肉")]
    [SerializeField]
    float sourceType;
    [SerializeField]
    //資源含量
    float sourceReserve;
    float max_SourceReserve;
    //是否正在採集
    public bool isCollecting = false;
    [SerializeField]
    GameObject picture;
    [SerializeField]
    GameObject head_Cry;
    [SerializeField]
    GameObject head_Gas;
    [SerializeField]
    GameObject head_Food;
    [SerializeField]
    TMP_Text t_Source;
    [SerializeField]
    GameObject removeBtn;

    [SerializeField]
    float removeMultiple = 0.2f;
    //一個工人用掉的食物
    [SerializeField]
    float usedFood = 50;


    [SerializeField]
    GameObject crystal;
    [SerializeField]
    GameObject crystal_Working;
    [Header("每格礦石增加值")]
    [SerializeField]
    float crystal_Add = 4;

    [SerializeField]
    GameObject gas;
    [SerializeField]
    GameObject gas_Working;
    [Header("每格瓦斯增加值")]
    [SerializeField]
    float gas_Add = 4;

    [SerializeField]
    GameObject food;
    [SerializeField]
    GameObject food_Working;
    [Header("每格食物增加值")]
    [SerializeField]
    float food_Add = 5;

    [SerializeField]
    GameObject crystalDeadIdol;
    [SerializeField]
    GameObject gasDeadIdol;
    [SerializeField]
    GameObject foodDeadIdol;

    [Header("採集條")]
    [SerializeField]
    Image m_CollectionBar;

    float timeCount;
    void Start()
    {
        if (sourceType == 0)
        {
            sourceReserve = 200;
            max_SourceReserve = 200;
        }
        if (sourceType == 1)
        {
            sourceReserve = 240;
            max_SourceReserve = 240;
        }
        if (sourceType == 2)
        {
            sourceReserve = 250;
            max_SourceReserve = 250;
        }
    }

    void Update()
    {
        SourceAddAndSkin();
        CollectionBarCtrl();
    }
    void CollectionBarCtrl()
    {
        if (sourceReserve / max_SourceReserve < 1) 
        {
            m_CollectionBar.gameObject.SetActive(true);
            m_CollectionBar.fillAmount = sourceReserve / max_SourceReserve;
        }
    }
    void SourceAddAndSkin()
    {
        if (isCollecting == false)
        {
            if (sourceType == 0)
            {
                gameObject.tag = "Crystal";
                crystal.SetActive(true);
                crystal_Working.SetActive(false);
            }
            if (sourceType == 1)
            {
                gameObject.tag = "Gas";
                gas.SetActive(true);
                gas_Working.SetActive(false);
            }
            if (sourceType == 2)
            {
                gameObject.tag = "Food";
                food.SetActive(true);
                food_Working.SetActive(false);
            }
        }
        if (isCollecting == true)
        {
            if (sourceType == 0)
            {
                crystal.SetActive(true);
                crystal_Working.SetActive(true);

                t_Source.text = "工兵(採礦)";

                timeCount += Time.deltaTime;
                if (timeCount >= WorldControler.crystalAddCountSet)
                {
                    sourceReserve -= crystal_Add;
                    timeCount = 0;
                }
            }
            if (sourceType == 1)
            {
                gas.SetActive(true);
                gas_Working.SetActive(true);

                t_Source.text = "工兵(集氣)";

                timeCount += Time.deltaTime;
                if (timeCount >= WorldControler.crystalAddCountSet)
                {
                    sourceReserve -= gas_Add;
                    timeCount = 0;
                }
            }
            if (sourceType == 2)
            {
                food.SetActive(true);
                food_Working.SetActive(true);

                t_Source.text = "工兵(伐木)";

                timeCount += Time.deltaTime;
                if (timeCount >= WorldControler.crystalAddCountSet)
                {
                    sourceReserve -= food_Add;
                    timeCount = 0;
                }
            }
        }

        if(sourceReserve <= 0)
        {
            if (sourceType == 0)
            {
                WorldControler.crystalOutPut -= crystal_Add;
                Instantiate(crystalDeadIdol, transform.position + new Vector3(0.098f, 0, 0.36f), crystalDeadIdol.transform.rotation);
            }
            if (sourceType == 1)
            {
                WorldControler.gasOutPut -= gas_Add;
                Instantiate(gasDeadIdol, transform.position + new Vector3(0.4f, 0, 0.09f), gasDeadIdol.transform.rotation);
            }
            if (sourceType == 2)
            {
                WorldControler.foodOutPut -= food_Add;
                Instantiate(foodDeadIdol, transform.position + new Vector3(0.25f, 0, 0), foodDeadIdol.transform.rotation);
            }

            WorldControler.population -= 1;
            
            Destroy(gameObject);
        }
    }

    private void OnMouseDown()
    {
        if (isCollecting == true && !EventSystem.current.IsPointerOverGameObject())
        {
            GameObject[] UIs = GameObject.FindGameObjectsWithTag("UI");
            for (int i = 0; i < UIs.Length; i++)
            {
                UIs[i].SetActive(false);
            }

            picture.SetActive(true);
            removeBtn.SetActive(true);
            if(sourceType == 0)
            {
                head_Cry.SetActive(true);
                head_Gas.SetActive(false);
                head_Food.SetActive(false);
            }
            if (sourceType == 1)
            {
                head_Cry.SetActive(false);
                head_Gas.SetActive(true);
                head_Food.SetActive(false);
            }
            if (sourceType == 2)
            {
                head_Cry.SetActive(false);
                head_Gas.SetActive(false);
                head_Food.SetActive(true);
            }
        }
    }
    public void RemoveTower()
    {
        WorldControler.food += usedFood * removeMultiple;
        WorldControler.population -= 1;

        isCollecting = false;

        if (sourceType == 0)
        {
            WorldControler.crystalOutPut -= crystal_Add;
        }
        if (sourceType == 1)
        {
            WorldControler.gasOutPut -= gas_Add;
        }
        if (sourceType == 2)
        {
            WorldControler.foodOutPut -= food_Add;
        }

        picture.SetActive(false);
        removeBtn.SetActive(false);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Tower")
        {
            isCollecting = true;
            Destroy(other.gameObject.transform.parent.gameObject);
            WorldControler.buildType = 0;
            TowerReadyDeploy.isDepoling = false;

            //投入工兵產量增加
            if (sourceType == 0)
            {
                WorldControler.crystalOutPut += crystal_Add;
            }
            if (sourceType == 1)
            {
                WorldControler.gasOutPut += gas_Add;
            }
            if (sourceType == 2)
            {
                WorldControler.foodOutPut += food_Add;
            }
        }
    }
}
