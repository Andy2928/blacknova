using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlowClose : MonoBehaviour
{
    float timeCount;
    [SerializeField]
    float timeSet;
    void Update()
    {
        if(gameObject.GetComponent<SpriteGlow.SpriteGlowEffect>().EnableInstancing == false)
        {
            timeCount += Time.deltaTime;
            if(timeCount >= timeSet)
            {
                gameObject.GetComponent<SpriteGlow.SpriteGlowEffect>().EnableInstancing = true;
                timeCount = 0;
            }
        }
    }
}
