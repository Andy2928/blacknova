using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuCtrl : MonoBehaviour
{
    [SerializeField]
    int nowLevel;
    [SerializeField]
    GameObject menu;
    [SerializeField]
    Button exitBtn;
    [SerializeField]
    GameObject m_trasition;
    [SerializeField]
    GameObject exitGameWarn;
    bool menuSetActive;

    [SerializeField]
    Sprite lightBtn;
    [SerializeField]
    Sprite darkBtn;
    [SerializeField]
    bool isClickDown;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        menu.SetActive(menuSetActive);

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (menu.activeSelf == true)
            {
                ClickBackToGame();
            }
            else
            {
                MenuBtn();
            }
        }
        if (nowLevel == 1 && WorldControler.teachPass_One == false)
        {
            exitBtn.interactable = false;
        }
        else
        {
            exitBtn.interactable = true;
        }
    }
    public void MenuBtn()
    {
        Object Music_ChangePage = Resources.Load("Music_ChangePage");
        Instantiate(Music_ChangePage);
        menuSetActive = !menuSetActive;
    }
    public void ClickBackToGame()
    {
        menuSetActive = !menuSetActive;
        WorldControler.timeStop = false;
    }
    public void ClickRestart()
    {
        m_trasition.GetComponent<TransitionCtrl>().goToLevel = nowLevel;
        m_trasition.SetActive(true);
        WorldControler.timeStop = false;
    }
    public void ClickExitLevel()
    {
        m_trasition.GetComponent<TransitionCtrl>().goToLevel = 2;
        m_trasition.SetActive(true);
        WorldControler.timeStop = false;
    }
    public void GoToTitle()
    {
        m_trasition.GetComponent<TransitionCtrl>().goToLevel = 0;
        m_trasition.SetActive(true);
        WorldControler.timeStop = false;
    }
    public void ClickExitGame()
    {
        exitGameWarn.SetActive(true);
    }
    public void ExitGame()
    {
        Application.Quit();
    }
    public void DontExitGame()
    {
        exitGameWarn.SetActive(false);
    }

    public void ChangeBtnImage()
    {
        isClickDown = !isClickDown;
        if (isClickDown)
        {
            gameObject.GetComponent<Image>().sprite = darkBtn;
        }
        if (!isClickDown)
        {
            gameObject.GetComponent<Image>().sprite = lightBtn;
        }
    }
}
