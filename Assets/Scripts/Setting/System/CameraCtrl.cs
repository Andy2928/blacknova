using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraCtrl : MonoBehaviour
{
    [SerializeField]
    Image side_Top;
    [SerializeField]
    Image side_Btm;
    [SerializeField]
    Image side_Left;
    [SerializeField]
    Image side_Right;

    [SerializeField]
    float x_Max;
    [SerializeField]
    float x_Min;
    [SerializeField]
    float y_Max;
    [SerializeField]
    float y_Min;

    [SerializeField]
    float speed;

    void Update()
    {
        if (unimaskCtrl.unimaskdown)
        {
            if (transform.position.z > y_Max)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, y_Max);
            }
            if (transform.position.z < y_Min)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, y_Min);
            }
            if (transform.position.x > x_Max)
            {
                transform.position = new Vector3(x_Max, transform.position.y, transform.position.z);
            }
            if (transform.position.x < x_Min)
            {
                transform.position = new Vector3(x_Min, transform.position.y, transform.position.z);
            }

            if (DialogSystem.isTalking == false)
            {
                bool mouseOnSide_Top = RectTransformUtility.RectangleContainsScreenPoint(side_Top.rectTransform, Input.mousePosition);
                if (mouseOnSide_Top == true)
                {
                    if (transform.position.z < y_Max)
                    {
                        transform.position += Vector3.forward * Time.deltaTime * speed;
                        if (WorldControler.buildType != 0)
                        {
                            transform.position += Vector3.forward * Time.deltaTime * speed * 5;
                        }
                    }
                }
                bool mouseOnSide_Btm = RectTransformUtility.RectangleContainsScreenPoint(side_Btm.rectTransform, Input.mousePosition);
                if (mouseOnSide_Btm == true)
                {
                    if (transform.position.z > y_Min)
                    {
                        transform.position += Vector3.back * Time.deltaTime * speed;
                        if (WorldControler.buildType != 0)
                        {
                            transform.position += Vector3.back * Time.deltaTime * speed * 5;
                        }
                    }
                }
                bool mouseOnSide_Right = RectTransformUtility.RectangleContainsScreenPoint(side_Right.rectTransform, Input.mousePosition);
                if (mouseOnSide_Right == true)
                {
                    if (transform.position.x < x_Max)
                    {
                        transform.position += Vector3.right * Time.deltaTime * speed;
                        if (WorldControler.buildType != 0)
                        {
                            transform.position += Vector3.right * Time.deltaTime * speed * 5;
                        }
                    }
                }
                bool mouseOnSide_Left = RectTransformUtility.RectangleContainsScreenPoint(side_Left.rectTransform, Input.mousePosition);
                if (mouseOnSide_Left == true)
                {
                    if (transform.position.x > x_Min)
                    {
                        transform.position += Vector3.left * Time.deltaTime * speed;
                        if (WorldControler.buildType != 0)
                        {
                            transform.position += Vector3.left * Time.deltaTime * speed * 5;
                        }
                    }
                }
            }
        }
    }
}