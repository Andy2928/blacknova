using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeachPiture : MonoBehaviour
{
    [SerializeField]
    float teachNumber;
    [SerializeField]
    GameObject m_ImmediateDialog;
    [SerializeField]
    GameObject m_Next;
    [SerializeField]
    GameObject m_Last;
    [SerializeField]
    GameObject m_Teach;
    [SerializeField]
    GameObject unimask;

    public void NextTeach()
    {
        Object Music_ChangePage = Resources.Load("Music_ChangePage");
        Instantiate(Music_ChangePage);
        m_Next.SetActive(true);
        gameObject.SetActive(false);
    }
    public void LastTeach()
    {
        Object Music_ChangePage = Resources.Load("Music_ChangePage");
        Instantiate(Music_ChangePage);
        m_Last.SetActive(true);
        gameObject.SetActive(false);
    }
    public void OverTeach()
    {
        if (teachNumber == 1.1f)
        {
            WorldControler.workerBuildTeach = true;
            m_ImmediateDialog.SetActive(true);
            unimask.SetActive(true);
        }
        if (teachNumber == 1.15f)
        {
            unimask.SetActive(true);
        }
        if (teachNumber == 1.2f)
        {
            WorldControler.houseBuildTeach = true;
            m_ImmediateDialog.SetActive(true);
            unimask.SetActive(true);
        }
        if (teachNumber == 1.3f)
        {
            WorldControler.outpostBuildTeach = true;
            unimask.SetActive(true);
        }
        if (teachNumber == 1.4f)
        {
            WorldControler.defenseTeach = true;
            unimask.SetActive(true);
        }
        if (teachNumber == 1.5f)
        {
            WorldControler.eliteTeach = true;
        }
        if (teachNumber == 1.6f)//����
        {
            WorldControler.b_levelTwoStart = true;
            m_ImmediateDialog.SetActive(true);
        }
        //if (teachNumber == 1.7f)//�ܫ}
        //{
        //    WorldControler.b_levelThreeStart = true;
        //    m_ImmediateDialog.SetActive(true);
        //}
        if (teachNumber == 1.8f)//�ܲ�
        {
            m_ImmediateDialog.SetActive(true);
        }
        m_Teach.SetActive(false);
        WorldControler.timeStop = false;
    }
}