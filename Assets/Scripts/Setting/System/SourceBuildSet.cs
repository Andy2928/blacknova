using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SourceBuildSet : MonoBehaviour
{
    [Header("資源種類,0礦1瓦斯2肉")]
    [SerializeField]
    float sourceType;
    //是否正在採集
    public bool isCollecting = false;
    [SerializeField]
    GameObject picture;
    [SerializeField]
    GameObject head_Cry;
    [SerializeField]
    GameObject head_Gas;
    [SerializeField]
    GameObject head_Food;
    [SerializeField]
    TMP_Text t_Source;
    [SerializeField]
    GameObject removeBtn;

    [SerializeField]
    float removeMultiple = 0.2f;
    //一個工人用掉的食物
    [SerializeField]
    float usedFood = 50;


    [SerializeField]
    GameObject crystal_Working;
    [Header("每格礦石增加值")]
    [SerializeField]
    float crystal_Add = 3;

    [SerializeField]
    GameObject gas_Working;
    [Header("每格瓦斯增加值")]
    [SerializeField]
    float gas_Add = 2;

    [SerializeField]
    GameObject food_Working;
    [Header("每格食物增加值")]
    [SerializeField]
    float food_Add = 3;

    float timeCount;

    void Update()
    {
        SourceAddAndSkin();
    }
    void SourceAddAndSkin()
    {
        if (isCollecting == false)
        {
            if (sourceType == 0)
            {
                gameObject.tag = "Crystal";
                crystal_Working.SetActive(false);
            }
            if (sourceType == 1)
            {
                gameObject.tag = "Gas";
                gas_Working.SetActive(false);
            }
            if (sourceType == 2)
            {
                gameObject.tag = "Food";
                food_Working.SetActive(false);
            }
        }
        if (isCollecting == true)
        {
            if (sourceType == 0)
            {
                crystal_Working.SetActive(true);

                t_Source.text = "工兵(採礦)";

                timeCount += Time.deltaTime;
                if (timeCount >= WorldControler.crystalAddCountSet)
                {
                    timeCount = 0;
                }
            }
            if (sourceType == 1)
            {
                gas_Working.SetActive(true);

                t_Source.text = "工兵(集氣)";

                timeCount += Time.deltaTime;
                if (timeCount >= WorldControler.crystalAddCountSet)
                {
                    timeCount = 0;
                }
            }
            if (sourceType == 2)
            {
                food_Working.SetActive(true);

                t_Source.text = "工兵(伐木)";

                timeCount += Time.deltaTime;
                if (timeCount >= WorldControler.crystalAddCountSet)
                {
                    timeCount = 0;
                }
            }
        }
    }

    private void OnMouseDown()
    {
        if (isCollecting == true)
        {
            GameObject[] UIs = GameObject.FindGameObjectsWithTag("UI");
            for (int i = 0; i < UIs.Length; i++)
            {
                UIs[i].SetActive(false);
            }

            picture.SetActive(true);
            removeBtn.SetActive(true);
            if (sourceType == 0)
            {
                head_Cry.SetActive(true);
                head_Gas.SetActive(false);
                head_Food.SetActive(false);
            }
            if (sourceType == 1)
            {
                head_Cry.SetActive(false);
                head_Gas.SetActive(true);
                head_Food.SetActive(false);
            }
            if (sourceType == 2)
            {
                head_Cry.SetActive(false);
                head_Gas.SetActive(false);
                head_Food.SetActive(true);
            }
        }
    }
    public void RemoveTower()
    {
        WorldControler.food += usedFood * removeMultiple;
        WorldControler.population -= 1;

        isCollecting = false;

        if (sourceType == 0)
        {
            WorldControler.crystalOutPut -= crystal_Add;
        }
        if (sourceType == 1)
        {
            WorldControler.gasOutPut -= gas_Add;
        }
        if (sourceType == 2)
        {
            WorldControler.foodOutPut -= food_Add;
        }

        picture.SetActive(false);
        removeBtn.SetActive(false);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.name == "Worker")
        {
            isCollecting = true;
            Destroy(other.gameObject.transform.parent.gameObject);
            WorldControler.buildType = 0;
            TowerReadyDeploy.isDepoling = false;

            //投入工兵產量增加
            if (sourceType == 0)
            {
                WorldControler.crystalOutPut += crystal_Add;
            }
            if (sourceType == 1)
            {
                WorldControler.gasOutPut += gas_Add;
            }
            if (sourceType == 2)
            {
                WorldControler.foodOutPut += food_Add;
            }
        }
    }
}
