using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TransitionCtrl : MonoBehaviour
{
    [SerializeField]//同DialogCtrl thisLevel
    int thisLevel;
    public int goToLevel;
    [SerializeField]
    float setSpeed;
    [SerializeField]
    float reboundSpeed;
    float speed;
    bool b_Close;//第一次闔上
    bool b_Rebound;//回彈
    bool b_FinalWait;
    [SerializeField]
    GameObject door_L;
    [SerializeField]
    GameObject door_R;
    [SerializeField]
    float transitionWait = 1;
    float timeCount;

    [SerializeField]
    GameObject dialogSystem;
    [SerializeField]
    MainMenuCtrl m_MainMenu;
    [SerializeField]
    GameObject m_ImmediateDialog;
    [SerializeField]
    GameObject teachInStart;

    [SerializeField]
    GameObject music_Open;
    [SerializeField]
    GameObject music_Close;

    bool startResourceSet = false;

    private void OnEnable()
    {
        if (WorldControler.b_Transition == false)
        {
            door_L.transform.localPosition = new Vector3(-1100, 0, 0);
            door_R.transform.localPosition = new Vector3(1100, 0, 0);
        }
        if (WorldControler.b_Transition == true)
        {
            door_L.transform.localPosition = new Vector3(0, 0, 0);
            door_R.transform.localPosition = new Vector3(0, 0, 0);
        }
    }

    void Update()
    {
        if (b_Rebound == false)
        {
            speed = setSpeed;
        }
        if (b_Rebound == true)
        {
            speed = setSpeed * reboundSpeed;
        }
        if (WorldControler.b_Transition == false)
        {
            music_Close.SetActive(true);
            if (door_L.transform.localPosition.x > 0 && door_R.transform.localPosition.x < 0 && b_Close == false)
            {
                b_Close = true;
            }
            if (door_L.transform.localPosition.x < -50 && door_R.transform.localPosition.x > 50 && b_Close == true)
            {
                b_Rebound = true;
            }
            if(door_L.transform.localPosition.x >= 0 && door_R.transform.localPosition.x <= 0 && b_Rebound == true)
            {
                b_FinalWait = true;
            }
            if(b_FinalWait == true)
            {
                door_L.transform.localPosition = new Vector3(0, 0, 0);
                door_R.transform.localPosition = new Vector3(0, 0, 0);
                timeCount += Time.deltaTime;
                if(timeCount >= transitionWait)
                {
                    WorldControler.b_Transition = true;
                    SceneManager.LoadScene(goToLevel);
                }
            }

            if (b_Close == false)
            {
                door_L.transform.localPosition += Vector3.right * Time.deltaTime * speed;
                door_R.transform.localPosition += Vector3.left * Time.deltaTime * speed;
            }
            else if (b_Rebound == false)
            {
                door_L.transform.localPosition += Vector3.left * Time.deltaTime * speed;
                door_R.transform.localPosition += Vector3.right * Time.deltaTime * speed;
            }
            else if (b_FinalWait == false)
            {
                door_L.transform.localPosition += Vector3.right * Time.deltaTime * speed;
                door_R.transform.localPosition += Vector3.left * Time.deltaTime * speed;
            }
        }


        if (WorldControler.b_Transition == true)
        {
            music_Open.SetActive(true);

            door_L.transform.localPosition += Vector3.left * Time.deltaTime * speed;
            door_R.transform.localPosition += Vector3.right * Time.deltaTime * speed;

            if (door_L.transform.localPosition.x < -1100 && door_R.transform.localPosition.x > 1100)
            {
                if (dialogSystem.activeSelf == false && startResourceSet == false)//如果沒有主對話，資源緩慢增加設置
                {
                    WorldControler.crystalOutPut = 1;
                    WorldControler.gasOutPut = 1;
                    WorldControler.foodOutPut = 1;
                    startResourceSet = true;
                }

                WorldControler.b_Transition = false;
                if(thisLevel == 1 && WorldControler.plot_2 == false)
                {
                    dialogSystem.SetActive(true);
                }
                else if (thisLevel == 1 && WorldControler.plot_3 == false)
                {
                    dialogSystem.SetActive(true);
                }
                else if (thisLevel == 1 && WorldControler.isTalk6 == false) 
                {
                    WorldControler.isTalk6 = true;
                    //m_ImmediateDialog.SetActive(true);
                }
                else if (thisLevel == 2 && WorldControler.pass_Two == false)
                {
                    teachInStart.SetActive(true);
                }
                else if (thisLevel == 1 && WorldControler.plot_4 == false && WorldControler.pass_Two == true)
                {
                    dialogSystem.SetActive(true);
                }
                else if (thisLevel == 1 && WorldControler.plot_5 == false && WorldControler.pass_Two == true)
                {
                    dialogSystem.SetActive(true);
                }
                else if (thisLevel == 1 && WorldControler.plot_6 == false && WorldControler.pass_Two == true)
                {
                    dialogSystem.SetActive(true);
                }
                else if (thisLevel == 3 && WorldControler.pass_Three == false)
                {
                    teachInStart.SetActive(true);
                }
                else if (thisLevel == 4 && WorldControler.pass_Four == false)
                {
                    teachInStart.SetActive(true);
                }
                else if (thisLevel == 1 && WorldControler.plot_9 == false && WorldControler.pass_Four == true)
                {
                    dialogSystem.SetActive(true);
                }
                else if (thisLevel == 1 && WorldControler.plot_10 == false && WorldControler.pass_Four == true)
                {
                    dialogSystem.SetActive(true);
                }
                else if (thisLevel == 1 && WorldControler.plot_11 == false && WorldControler.pass_Four == true)
                {
                    dialogSystem.SetActive(true);
                }
                gameObject.SetActive(false);
            }
        }
    }
}
