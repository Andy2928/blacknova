using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GoToChooseLevelBtn : MonoBehaviour
{
    [SerializeField]
    Button m_Btn;
    [SerializeField]
    GameObject m_Transition;
    private void Update()
    {
        if (WorldControler.teachPass_One == false)
        {
            m_Btn.interactable = false;
        }
    }
    public void OnClick()
    {
        m_Transition.GetComponent<TransitionCtrl>().goToLevel = 2;
        m_Transition.SetActive(true);
    }
}
