using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EliteListTeach : MonoBehaviour
{
    [SerializeField]
    GameObject eliteListTeach;
    private void OnEnable()
    {
        if(WorldControler.teachEliteList == false)
        {
            eliteListTeach.SetActive(true);
            WorldControler.teachEliteList = true;
        }
    }
}
