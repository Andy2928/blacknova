using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class unimaskCtrl : MonoBehaviour
{
    [SerializeField]
    GameObject nextGameObject;

    [SerializeField]
    bool isNeedCall;
    [SerializeField]
    bool isCallCastle;
    [SerializeField]
    bool isCallFactory;
    [SerializeField]
    bool isCallXiaLei;
    [SerializeField]
    bool isFree;
    [SerializeField]
    GameObject callThing;

    public static bool unimaskdown;

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Mouse0) && !isCallCastle && !isCallFactory && !isCallXiaLei)
        {
            if (nextGameObject != null)
            {
                nextGameObject.SetActive(true);
            }
            gameObject.SetActive(false);
            if(isFree)
            {
                unimaskdown = true;
            }
            if (isNeedCall)
            {
                callThing.SetActive(true);
            }
        }
        else if(isCallCastle && Input.GetKeyDown(KeyCode.Alpha1) || isCallFactory && Input.GetKeyDown(KeyCode.Alpha2) || isCallXiaLei && Input.GetKeyDown(KeyCode.Alpha5))
        {
            if (nextGameObject != null)
            {
                nextGameObject.SetActive(true);
            }
            gameObject.SetActive(false);
            if (isNeedCall)
            {
                callThing.SetActive(true);
            }
        }
    }

    //public void OnClick()
    //{
    //    if (nextGameObject != null && !isCallCastle && !isCallFactory && !isCallXiaLei) 
    //    {
    //        nextGameObject.SetActive(true);
    //    }
    //    gameObject.SetActive(false);

    //    if(isNeedCall)
    //    {
    //        callThing.SetActive(true);
    //    }
    //}
}
