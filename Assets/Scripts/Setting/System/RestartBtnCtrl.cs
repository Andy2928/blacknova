using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartBtnCtrl : MonoBehaviour
{
    public int nowLevel;
    public GameObject m_Transition;

    // Update is called once per frame
    public void OnClick()
    {
        m_Transition.GetComponent<TransitionCtrl>().goToLevel = nowLevel;
        m_Transition.SetActive(true);
    }
}
