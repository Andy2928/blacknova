using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MainMenuCtrl : MonoBehaviour
{
    [SerializeField]
    int nowLevel;
    public static bool openFunction;
    public int nowFunction;//0選關1清單
    public int nowChooseLevel;
    int levelUnlock;//根據關卡進度解鎖地圖
    [SerializeField]
    GameObject m_Mask;
    [SerializeField]
    GameObject functions;
    [SerializeField]
    Image levelPage;
    [SerializeField]
    Image levelBackGround;
    [SerializeField]
    Sprite[] levelPageImage;
    [SerializeField]
    Sprite[] levelBackGroundImage;
    [SerializeField]
    TransitionCtrl tc_Transition;
    [SerializeField]
    GameObject m_Transition;

    [SerializeField]
    GameObject chooseLevel;
    [SerializeField]
    GameObject memberList;

    [SerializeField]
    TMP_Text t_Money;


    private void OnEnable()
    {
        if (openFunction == false)
        {
            functions.SetActive(false);
        }
        else
        {
            functions.SetActive(true);
        }

        if (WorldControler.pass_Three == true)
        {
            levelUnlock = 3;
        }
        else if(WorldControler.pass_Two == true)
        {
            levelUnlock = 2;
        }
        else if(WorldControler.teachPass_One == true)
        {
            levelUnlock = 1;
        }
        nowFunction = 0;
        nowChooseLevel = levelUnlock;
        m_Transition.SetActive(true);
    }
    void Update()
    {
        if (nowFunction == 0)
        {
            chooseLevel.SetActive(true);
            memberList.SetActive(false);
        }
        if (nowFunction == 1)
        {
            chooseLevel.SetActive(false);
            memberList.SetActive(true);
        }

        levelBackGround.sprite = levelBackGroundImage[nowChooseLevel];
        levelPage.sprite = levelPageImage[nowChooseLevel];
        if(Input.GetKeyDown(KeyCode.A))
        {
            LevelReduce();
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            LevelAdd();
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            FunctionReduce();
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            FunctionAdd();
        }
    }
    public void LevelAdd()
    {
        if (nowFunction == 0)
        {
            Object Music_PlanteChoose = Resources.Load("Music_PlanteChoose");
            Instantiate(Music_PlanteChoose);

            nowChooseLevel += 1;
            m_Mask.SetActive(false);
            m_Mask.SetActive(true);
            if (nowChooseLevel > levelUnlock)
            {
                nowChooseLevel = 0;
            }
        }
    }
    public void LevelReduce()
    {
        if (nowFunction == 0)
        {
            Object Music_PlanteChoose = Resources.Load("Music_PlanteChoose");
            Instantiate(Music_PlanteChoose);

            nowChooseLevel -= 1;
            m_Mask.SetActive(false);
            m_Mask.SetActive(true);
            if (nowChooseLevel < 0)
            {
                nowChooseLevel = levelUnlock;
            }
        }
    }

    public void FunctionAdd()
    {
        nowFunction += 1;
        if (nowFunction > 1)
        {
            nowFunction = 0;
        }

    }
    public void FunctionReduce()
    {
        nowFunction -= 1;
        if (nowFunction < 0)
        {
            nowFunction = 1;
        }

    }
    public void GoToLevel()
    {
        Object Music_PlantePress = Resources.Load("Music_PlantePress");
        Instantiate(Music_PlantePress);

        if (nowChooseLevel == 0)
        {
            tc_Transition.goToLevel = 1;
        }
        if (nowChooseLevel == 1)
        {
            tc_Transition.goToLevel = 3;
        }
        if (nowChooseLevel == 2)
        {
            tc_Transition.goToLevel = 4;
        }
        if (nowChooseLevel == 3)
        {
            tc_Transition.goToLevel = 5;
        }
        WorldControler.b_Transition = false;
        m_Transition.SetActive(true);
    }
}
