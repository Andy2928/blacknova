using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
public class LongPressEffect : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField]
    bool cantUse;
    [SerializeField]
    GameObject t_Note;

    public float PressDownTimer; //按下幾秒觸發
    private bool PressDown; //按下
    public UnityEvent onLongClick; //開啟Inspector觸發事件
    [SerializeField]
    public float HoldTime;

    public bool techUnlock;

    [SerializeField]
    bool isCastle;
    [SerializeField]
    int techLevel;
    [SerializeField]
    float needMoney;

    public static int nowCastleLevel;
    [SerializeField]
    Image levelUpSlider;
    [Header("1輕2科3魔4傭5重6挖7鑽8栽9條")]
    [SerializeField]
    int techType;
    [SerializeField]
    LongPressEffect preTech;
    [SerializeField]
    GameObject m_ImmediateDialog;
    [SerializeField]
    GameObject m_ProductionBuildTeach;
    //按下按鈕

    [Header("音效")]
    [SerializeField]
    GameObject music_TechUping;
    [SerializeField]
    GameObject music_TechUpDown;

    public void OnPointerDown(PointerEventData eventData)
    {
        PressDown = true;
    }
    //按鈕彈起
    public void OnPointerUp(PointerEventData eventData)
    {
        Reset();
    }
    void Check()
    {
        if(WorldControler.productionBuildUnlock == true && isCastle == true && techLevel == 2)
        {
            levelUpSlider.fillAmount = 1;
            techUnlock = true;
        }

        if (WorldControler.empUnlock == true && techType == 2 && techLevel == 1)
        {
            levelUpSlider.fillAmount = 1;
            techUnlock = true;
        }
    }
    //當按下按鈕 PressDown = true 時計時
    private void Start()
    {
        Check();
        if (nowCastleLevel == 0)
        {
            nowCastleLevel = 1;
        }
    }
    void Update()
    {
        //顯示說明欄
        bool mouseOnBtn = RectTransformUtility.RectangleContainsScreenPoint(gameObject.GetComponent<Image>().rectTransform, Input.mousePosition);
        if (mouseOnBtn == true && t_Note != null)
        {
            t_Note.SetActive(true);
        }
        if (mouseOnBtn == false && t_Note != null)
        {
            t_Note.SetActive(false);
        }

        if (cantUse == true)
        {
            gameObject.GetComponent<Image>().color = Color.gray;
        }
        if (cantUse == false)
        {
            if (techType == 2 && WorldControler.pass_Two == false || WorldControler.money < needMoney)
            {
                gameObject.GetComponent<Image>().color = Color.gray;
            }
            else
            {
                gameObject.GetComponent<Image>().color = Color.white;
                if (PressDown == true)
                {
                    PressDownTimer += Time.deltaTime;

                    if (preTech != null && preTech.techUnlock == false)
                    { }
                    else
                    {
                        if (isCastle == true && techLevel == nowCastleLevel + 1 && WorldControler.money >= needMoney)
                        {
                            music_TechUping.SetActive(true);
                            levelUpSlider.fillAmount = (PressDownTimer / HoldTime);
                        }
                        if (isCastle == false && techLevel <= nowCastleLevel && WorldControler.money >= needMoney)
                        {
                            music_TechUping.SetActive(true);
                            levelUpSlider.fillAmount = (PressDownTimer / HoldTime);
                        }
                    }

                    if (PressDownTimer >= HoldTime)
                    {
                        music_TechUpDown.SetActive(true);
                        WorldControler.money -= needMoney;
                        LongPressFuntion();
                        Reset();
                    }
                }

                if (techUnlock == true)
                {
                    levelUpSlider.fillAmount = 1;
                }
            }
            //if (isCastle == true && techLevel > nowCastleLevel + 1 || WorldControler.money < needMoney || isCastle == false && techLevel > nowCastleLevel)
            //{
            //    gameObject.GetComponent<Image>().sprite = cantUseImage;
            //}
            //else if(techUnlock == true)
            //{
            //    gameObject.GetComponent<Image>().sprite = normalImage;
            //    levelUpSlider.fillAmount = 1;
            //}
            //else
            //{
            //    gameObject.GetComponent<Image>().sprite = normalImage;
            //}
        }
    }
    //當PressUp的時候重製計算時間
    private void Reset()
    {
        music_TechUping.SetActive(false);
        if (techUnlock == false)
        {
            levelUpSlider.fillAmount = 0;
        }
        PressDown = false;
        PressDownTimer = 0;
    }
    void LongPressFuntion()
    {
        if (onLongClick != null)
        {
            techUnlock = true;
            if (isCastle == true)
            {
                nowCastleLevel += 1;
                if (techLevel == 2)
                {
                    WorldControler.productionBuildUnlock = true;
                    m_ProductionBuildTeach.SetActive(true);
                }
            }
            else
            {
                if (techLevel == 1 && techType == 2)
                {
                    WorldControler.empUnlock = true;
                    m_ProductionBuildTeach.SetActive(true);
                }
            }
        }
    }
}
