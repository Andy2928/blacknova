using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class WorldControler : MonoBehaviour
{
    bool cheatOn;

    [SerializeField]
    int nowLevel;
    [SerializeField]
    GameObject castle;
    [SerializeField]
    GameObject enemyIns;
    [SerializeField]
    GameObject loseUI;
    [SerializeField]
    GameObject winUI;
    [SerializeField]
    GameObject m_Dialog;
    //是否需要在結算前對話
    [Header("是否需要在結算前對話")]
    [SerializeField]
    bool b_needFinalDialog;
    //是否在結算前對話過
    bool isFinalDialog;
    //敵人是否清空
    public static bool isEnemyClear;

    GameObject enemys;

    public static GameObject nowBuildBarrack;


    public static bool timeStop;
    [SerializeField]
    [Header("時間暫停頁面")]
    GameObject stopUI;
    float worldTimeLevel = 1;
    [SerializeField]
    [Header("建造中時間減緩")]
    float buildTimeSlow = 0.2f;
    [SerializeField]
    bool isSpeedUp;
    [SerializeField]
    [Header("世界速度LV.1")]
    float worldTimeSpeed_1 = 1;
    [SerializeField]
    [Header("世界速度LV.2")]
    float worldTimeSpeed_2 = 2;
    [SerializeField]
    [Header("世界速度LV.3")]
    float worldTimeSpeed_3 = 3;
    [SerializeField]
    [Header("世界速度LV.MAX")]
    float worldTimeSpeed_Max = 10;

    ////資源
    //晶石========================================
    [Header("資源")]
    [Header("晶石")]
    [SerializeField]
    TMP_Text t_Crystal;
    public static float crystal = 0;
    //晶石過多久增加
    public static float crystalAddCountSet = 1;
    //晶石產量
    public static float crystalOutPut = 0;
    float crystalTimeCount;
    //燃氣========================================
    [Header("燃氣")]
    [SerializeField]
    TMP_Text t_Gas;
    public static float gas = 0;
    //燃氣過多久增加
    public static float gasAddCountSet = 1;
    //燃氣產量
    public static float gasOutPut = 0;
    float gasTimeCount;
    //肉樹========================================
    [Header("食物")]
    [SerializeField]
    TMP_Text t_Food;
    public static float food = 200;
    //食物過多久增加
    public static float foodAddCountSet = 1;
    //食物產量
    public static float foodOutPut = 0;
    float foodTimeCount;
    //金錢========================================
    [Header("金錢")]
    [SerializeField]
    TMP_Text t_Money;
    public static float money;
    //人口========================================
    [Header("人口")]
    [SerializeField]
    TMP_Text t_Population;
    public static float population = 0;
    public static float maxPopulation = 5;



    //當前建造塔的種類(後更改為已經選擇點擊"建造按鈕")
    public static float buildType;

    //建造狀態
    public static bool isDeploy;

    [Header("第一次教學")]
    [SerializeField]
    GameObject m_HouseTeach;

    //關卡初始化
    [Header("關卡初始化")]
    [SerializeField]
    float startCrystal;
    [SerializeField]
    float startFood;
    [SerializeField]
    float startGas;
    [SerializeField]
    float startPopulation;

    [Header("結算")]
    bool b_ToMoney;
    [SerializeField]
    TMP_Text finalCrystal;
    [SerializeField]
    TMP_Text finalFood;
    [SerializeField]
    TMP_Text finalGas;
    [SerializeField]
    TMP_Text finalMoney;
    [SerializeField]
    TMP_Text crystalToMoney;
    [SerializeField]
    TMP_Text foodToMoney;
    [SerializeField]
    TMP_Text gasToMoney;
    [SerializeField]
    GameObject m_transition;


    [Header("熱鍵設置")]
    [SerializeField]
    GameObject GameStop;
    [SerializeField]
    Color32 c_Gray;
    [SerializeField]
    Image m_CastleKey;
    public static bool save_Castle;
    [SerializeField]
    Image m_FactoryKey;
    public static bool save_Factory;
    [SerializeField]
    Image m_TechKey;
    public static bool save_Tech;
    [SerializeField]
    Image m_MagicKey;
    public static bool save_Magic;
    [SerializeField]
    Image m_XiaLeiKey;
    public static bool save_XiaLei;
    [SerializeField]
    Image m_LeiMiKey;
    public static bool save_Limey;
    [SerializeField]
    Image m_SaFenTeKey;
    public static bool save_SaFenTe;



    //flags
    //通關紀錄
    public static bool teachPass_One;//第一關通關
    public static bool pass_Two;//第二關通關
    public static bool pass_Three;//第三關通關
    public static bool pass_Four;//第四關通關

    //第一關教學&對話
    public static bool workerBuildTeach;//教學1結束後開啟，觸發對話1(plot_1)
    public static bool houseBuildTeach;//教學2結束後開啟，觸發對話2，解鎖宿舍
    public static bool houseBuilded;//建造完宿舍開啟，觸發教學3
    public static bool outpostBuildTeach;//教學3結束後開啟，解鎖哨塔
    public static bool outpostBuiled;//建造完哨塔開啟，觸發對話3
    public static bool defenseTeach;//教學4結束後開啟，5秒後..
    public static bool isTalk4;//觸發對話4和箭頭，箭頭出現到40秒。
    public static bool isTalk5;//80秒時觸發對話5，對話5結束後觸發教學5
    public static bool eliteTeach;//教學5結束後開啟，解鎖夏蕾
    //主選單教學&對話
    public static bool b_Transition;//轉場控制bool
    public static bool plot_2;//"主場景"觸發劇情對話2，劇情對話2結束後，過場，開啟"場景2圖片"
    public static bool plot_3;//觸發劇情對話3，劇情對話3結束後，過場，切換到科技樹場景
    public static bool isTalk6;//結束後開啟科技樹教學
    //public static bool teachTechTree;//科技樹教學
    public static bool teachEliteList;//菁英頁面教學
    //第二關劇情
    public static bool b_levelTwoStart;//第二關起始對話
    public static bool b_levelTwoEnd;//第二關結束對話
    //第二關轉場劇情對話
    public static bool plot_4;
    public static bool plot_5;
    public static bool plot_6;
    //第三關劇情
    public static bool b_levelThreeStart;//第三關起始教學，還有解鎖萊咪
    public static bool b_levelThreeEnd;//第三關結束對話
    //第四關劇情
    public static bool b_levelFourStart;//第四關起始教學，還有解鎖萊咪
    public static bool b_levelFourEnd;//第四關結束對話
    public static bool plot_9;
    public static bool plot_10;
    public static bool plot_11;


    //解鎖
    public static bool LabUnlock;//解鎖生產建築的即時對話，開啟菁英、選關畫面
    //科技
    public static bool productionBuildUnlock;//主堡LV2，解鎖生產建築的即時對話，開啟菁英、選關畫面
    public static bool empUnlock;//科研LV1，解鎖電磁兵

    //菁英升級
    public static bool ShalehLevelUp;//夏蕾升級符號
    public static float ShalehExp;
    public static bool ShalehLV_2;
    public static bool ShalehLV_3;
    public static bool ShalehLV_4;
    public static bool ShalehLV_5;
    public static bool ShalehLV_6;
    public static bool ShalehLV_7;
    public static bool ShalehLV_8;
    public static bool ShalehLV_9;

    public static bool LimeyLevelUp;//萊咪升級符號
    public static float LimeyExp;
    public static bool LimeyLV_2;
    public static bool LimeyLV_3;
    public static bool LimeyLV_4;
    public static bool LimeyLV_5;
    public static bool LimeyLV_6;
    public static bool LimeyLV_7;
    public static bool LimeyLV_8;
    public static bool LimeyLV_9;

    //測試鈕
    public bool isTest;

    private void Start()
    {
        if (isTest == false)
        {
            m_transition.SetActive(true);
        }
        LevelReset();
        if (nowLevel == 1)
        {
            gas += 100;
        }
    }
    void Update()
    {
        ResultJudge();
        SourceUpdate();
        ClickControler();
        TimeControl();
        Cheat();
        Teach();
        EliteLvUp();
        HotKey();
    }
    void HotKey()
    {
        //遊戲暫停
        if(Input.GetKeyDown(KeyCode.Space) && GameStop.activeSelf == false)
        {
            GameStop.SetActive(true);
        }
        if (save_Castle == true)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                GameObject.Find("Castle").GetComponent<CastleControler>().OnMouseDown();
            }
        }
        else
        {
            m_CastleKey.color = c_Gray;
        }
        if (save_Factory == true)
        {
            m_FactoryKey.color = Color.white;
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                GameObject.Find("LightFactory").GetComponent<TowerSetting>().OnMouseDown();
            }
        }
        else
        {
            m_FactoryKey.color = c_Gray;
        }
        if (save_Tech == true)
        {
            m_TechKey.color = Color.white;
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                GameObject.Find("Lab").GetComponent<TowerSetting>().OnMouseDown();
            }
        }
        else
        {
            m_TechKey.color = c_Gray;
        }
        if (save_Magic == true)
        {
            m_MagicKey.color = Color.white;
            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                GameObject.Find("MagicSchool").GetComponent<TowerSetting>().OnMouseDown();
            }
        }
        else
        {
            m_MagicKey.color = c_Gray;
        }

        if (save_XiaLei == true)
        {
            m_XiaLeiKey.color = Color.white;
            if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                GameObject.Find("XiaLei").GetComponent<TowerSetting>().OnMouseDown();
            }
        }
        else
        {
            m_XiaLeiKey.color = c_Gray;
        }
        if (save_Limey == true)
        {
            m_LeiMiKey.color = Color.white;
            if (Input.GetKeyDown(KeyCode.Alpha6))
            {
                GameObject.Find("Limey").GetComponent<TowerSetting>().OnMouseDown();
            }
        }
        else
        {
            m_LeiMiKey.color = c_Gray;
        }
        if (save_SaFenTe == true)
        {
            m_SaFenTeKey.color = Color.white;
            if (Input.GetKeyDown(KeyCode.Alpha7))
            {
                GameObject.Find("SaFenTe").GetComponent<TowerSetting>().OnMouseDown();
            }
        }
        else
        {
            m_SaFenTeKey.color = c_Gray;
        }
    }
    void EliteLvUp()
    {
        //夏蕾
        if (ShalehLV_2 == false && ShalehExp >= 20)
        {
            ShalehLevelUp = true;
            ShalehLV_2 = true;
            ShalehExp = 0;
        }
        if (ShalehLV_3 == false && ShalehExp >= 30)
        {
            ShalehLevelUp = true;
            ShalehLV_3 = true;
            ShalehExp = 0;
        }
        if (ShalehLV_4 == false && ShalehExp >= 50)
        {
            ShalehLevelUp = true;
            ShalehLV_4 = true;
            ShalehExp = 0;
        }
        if (ShalehLV_5 == false && ShalehExp >= 80)
        {
            ShalehLevelUp = true;
            ShalehLV_5 = true;
            ShalehExp = 0;
        }
        if (ShalehLV_6 == false && ShalehExp >= 120)
        {
            ShalehLevelUp = true;
            ShalehLV_6 = true;
            ShalehExp = 0;
        }
        if (ShalehLV_7 == false && ShalehExp >= 170)
        {
            ShalehLevelUp = true;
            ShalehLV_7 = true;
            ShalehExp = 0;
        }
        if (ShalehLV_8 == false && ShalehExp >= 230)
        {
            ShalehLevelUp = true;
            ShalehLV_8 = true;
            ShalehExp = 0;
        }
        if (ShalehLV_9 == false && ShalehExp >= 300)
        {
            ShalehLevelUp = true;
            ShalehLV_9 = true;
            ShalehExp = 0;
        }

        //蕾咪
        if (LimeyLV_2 == false && LimeyExp >= 2)
        {
            LimeyLevelUp = true;
            LimeyLV_2 = true;
            LimeyExp = 0;
        }
        if (LimeyLV_3 == false && LimeyExp >= 4)
        {
            LimeyLevelUp = true;
            LimeyLV_3 = true;
            LimeyExp = 0;
        }
        if (LimeyLV_4 == false && LimeyExp >= 6)
        {
            LimeyLevelUp = true;
            LimeyLV_4 = true;
            LimeyExp = 0;
        }
        if (LimeyLV_5 == false && LimeyExp >= 8)
        {
            LimeyLevelUp = true;
            LimeyLV_5 = true;
            LimeyExp = 0;
        }
        if (LimeyLV_6 == false && LimeyExp >= 11)
        {
            LimeyLevelUp = true;
            LimeyLV_6 = true;
            LimeyExp = 0;
        }
        if (LimeyLV_7 == false && LimeyExp >= 14)
        {
            LimeyLevelUp = true;
            LimeyLV_7 = true;
            LimeyExp = 0;
        }
        if (LimeyLV_8 == false && LimeyExp >= 17)
        {
            LimeyLevelUp = true;
            LimeyLV_8 = true;
            LimeyExp = 0;
        }
        if (LimeyLV_9 == false && LimeyExp >= 21)
        {
            LimeyLevelUp = true;
            LimeyLV_9 = true;
            LimeyExp = 0;
        }
    }
    void LevelReset()
    {
        //資源初始值
        crystal = startCrystal;
        gas = startGas;
        food = startFood;
        population = 0;
        maxPopulation = startPopulation;

        //Bool重置
        isEnemyClear = false;
        isFinalDialog = false;
        worldTimeLevel = 1;
        buildType = 0;
        isDeploy = false;
        b_ToMoney = false;

        //熱鍵重置
        save_Castle = true;
        save_Factory = false;
        save_XiaLei = false;
        save_Tech = false;
        save_Limey = false;
        save_Magic = false;
        save_SaFenTe = false;


        if (nowLevel == 1 && teachPass_One == false)
        {
            workerBuildTeach = false;
            houseBuildTeach = false;
            houseBuilded = false;
            outpostBuildTeach = false;
            outpostBuiled = false;
            defenseTeach = false;
            isTalk4 = false;
            isTalk5 = false;
            eliteTeach = false;
        }
        if (nowLevel == 2 && pass_Two == false)
        {
            b_levelTwoStart = false;
            b_levelTwoEnd = false;
        }
        if (nowLevel == 2 && pass_Two == false)
        {
            b_levelTwoStart = false;
            b_levelTwoEnd = false;
        }
        if (nowLevel == 1 && teachPass_One == true)
        {
            m_Dialog.SetActive(false);
            DialogSystem.isTalking = false;
        }
    }


    void Teach()
    {
        if(crystal >= 100 && houseBuildTeach == false)
        {
            m_HouseTeach.SetActive(true);
        }
    }
    //作弊
    void Cheat()
    {
        if (Input.GetKey(KeyCode.R) && Input.GetKey(KeyCode.O) && Input.GetKey(KeyCode.Y))
        {
            Debug.Log("開啟作弊");
            cheatOn = true;
        }
        if (cheatOn == true)//大量資源
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                crystal += 1000;
                gas += 1000;
                food += 1000;
                money += 10000;
                maxPopulation += 10;
            }
            if (Input.GetKeyDown(KeyCode.Alpha2))//跳關
            {
                teachPass_One = true;
                if (enemyIns != null)
                {
                    enemyIns.GetComponent<EnemyInsCtrl>().timeCount = 9999;
                }
            }
            if (Input.GetKeyDown(KeyCode.Alpha3))//跳過教學
            {
                workerBuildTeach = true;
                houseBuildTeach = true;
                houseBuilded = true;
                outpostBuildTeach = true;
                outpostBuiled = true;
                defenseTeach = true;
                isTalk4 = true;
                isTalk5 = true;
                eliteTeach = true;
            }
            if (Input.GetKeyDown(KeyCode.Alpha8))//通過第二關
            {
                pass_Two = true;
            }
            if (Input.GetKeyDown(KeyCode.Alpha9))//通過第三關
            {
                pass_Three = true;
            }
            if (Input.GetKeyDown(KeyCode.Alpha0))//通過第四關
            {
                pass_Four = true;
            }
        }
    }

    void ResultJudge()
    {
        enemys = GameObject.FindGameObjectWithTag("Enemy");

        if (castle != null && castle.GetComponent<CastleControler>().hp <= 0) 
        {
            CloseUI();
            loseUI.SetActive(true);
        }
        if (enemyIns != null && enemys == null && enemyIns.GetComponent<EnemyInsCtrl>().insFinal == true)
        {
            isEnemyClear = true;

            if (b_needFinalDialog == true)
            {
                if (isFinalDialog == false)
                {
                    CloseUI();
                    m_Dialog.SetActive(true);
                    isFinalDialog = true;
                }
                if (isFinalDialog == true && DialogSystem.isTalking == false)
                {
                    if (nowLevel == 2)
                    {
                        pass_Two = true;
                    }
                    if (nowLevel == 3)
                    {
                        pass_Three = true;
                    }

                    crystalOutPut = 0;
                    gasOutPut = 0;
                    foodOutPut = 0;

                    finalCrystal.text = crystal.ToString();
                    finalGas.text = gas.ToString();
                    finalFood.text = food.ToString();
                    crystalToMoney.text = (Mathf.Floor(crystal / 20) * 30).ToString() + "金錢";
                    gasToMoney.text = (Mathf.Floor(gas / 20) * 40).ToString() + "金錢";
                    foodToMoney.text = (Mathf.Floor(food / 20) * 20).ToString() + "金錢";
                    if (b_ToMoney == false)
                    {
                        money += (Mathf.Floor(crystal / 20) * 30) + (Mathf.Floor(gas / 20) * 40) + (Mathf.Floor(food / 20) * 20);
                        b_ToMoney = true;
                    }
                    finalMoney.text = money.ToString();
                    winUI.SetActive(true);
                }
            }
            else
            {
                if (nowLevel == 1)
                {
                    teachPass_One = true;
                }

                crystalOutPut = 0;
                gasOutPut = 0;
                foodOutPut = 0;

                finalCrystal.text = crystal.ToString();
                finalGas.text = gas.ToString();
                finalFood.text = food.ToString();
                crystalToMoney.text = (Mathf.Floor(crystal / 20) * 30).ToString() + "金錢";
                gasToMoney.text = (Mathf.Floor(gas / 20) * 40).ToString() + "金錢";
                foodToMoney.text = (Mathf.Floor(food / 20) * 20).ToString() + "金錢";
                if (b_ToMoney == false)
                {
                    money += (Mathf.Floor(crystal / 20) * 30) + (Mathf.Floor(gas / 20) * 40) + (Mathf.Floor(food / 20) * 20);
                    b_ToMoney = true;
                }
                finalMoney.text = money.ToString();
                winUI.SetActive(true);
            }
        }
    }

    //資源增加
    void SourceUpdate()
    {
        //晶礦
        crystalTimeCount += Time.deltaTime;
        if (crystalTimeCount >= crystalAddCountSet)
        {
            crystal += crystalOutPut;
            crystalTimeCount = 0;
        }
        t_Crystal.text = crystal.ToString();
        //瓦斯
        gasTimeCount += Time.deltaTime;
        if (gasTimeCount >= gasAddCountSet)
        {
            gas += gasOutPut;
            gasTimeCount = 0;
        }
        t_Gas.text = gas.ToString();
        //食物
        foodTimeCount += Time.deltaTime;
        if (foodTimeCount >= foodAddCountSet)
        {
            food += foodOutPut;
            foodTimeCount = 0;
        }
        t_Food.text = food.ToString();
        //錢
        t_Money.text = money.ToString();
        //人口
        t_Population.text = population + "/" + maxPopulation;
    }
    void CloseUI()
    {
        GameObject[] UIs = GameObject.FindGameObjectsWithTag("UI");
        for (int i = 0; i < UIs.Length; i++)
        {
            UIs[i].SetActive(false);
        }
    }
    void ClickControler()
    {
        //偵測點擊
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Input.GetMouseButtonDown(0) && Physics.Raycast(ray, out hit))
        {
            if (!CheckUI())
            {
                if (buildType == 0)
                {
                    if (hit.transform.tag == "Ground" || hit.transform.tag == "Grid")
                    {
                        CloseUI();
                    }
                }
                else
                {
                    if(hit.transform.tag == "CanBuild" && isDeploy == false)
                    {
                        isDeploy = true;
                    }
                }
            }
        }
    }
    void TimeControl()
    {
        if (timeStop == true)
        {
            Time.timeScale = 0;
        }
        if (timeStop == false)
        {
            if (buildType == 0)
            {
                Time.timeScale = 1 * worldTimeLevel;
            }
            else
            {
                Time.timeScale = buildTimeSlow;
            }
        }

        //if (Input.GetKeyDown(KeyCode.Keypad1))
        //{
        //    worldTimeLevel = worldTimeSpeed_1;
        //}
        //if (Input.GetKeyDown(KeyCode.Keypad2))
        //{
        //    worldTimeLevel = worldTimeSpeed_2;
        //}
        //if (Input.GetKeyDown(KeyCode.Keypad3))
        //{
        //    worldTimeLevel = worldTimeSpeed_3;
        //}
        //if (Input.GetKey(KeyCode.C) && Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.T))
        //{
        //    worldTimeLevel = worldTimeSpeed_Max;
        //}
    }
    public void SpeedUpBtn()
    {
        isSpeedUp = !isSpeedUp;

        if(isSpeedUp == false) 
        {
            worldTimeLevel = worldTimeSpeed_1;
        }
        else
        {
            worldTimeLevel = worldTimeSpeed_2;
        }
    }
    public void TimeStopBtn()
    {
        stopUI.SetActive(true);
    }

    private bool CheckUI()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }


    /////////////////////////////存檔
    public class SaveData
    {
        //金錢
        public float s_Money;
        //地圖進度
        //科技解鎖
        //強化
        public int s_NowCastleLevel;
    }
    SaveData SavingData()
    {
        var saveData = new SaveData();

        saveData.s_Money = money;
        saveData.s_NowCastleLevel = LongPressEffect.nowCastleLevel;

        return saveData;
    }

    void LoadData(SaveData saveData)
    {
        money = saveData.s_Money;
        LongPressEffect.nowCastleLevel = saveData.s_NowCastleLevel;
    }

    public void Save()
    {
        SaveSystem.SaveJson("PlayerData", SavingData());
    }
    public void Load()
    {
        var saveData = SaveSystem.LoadJson<SaveData>("PlayerData");
        LoadData(saveData);
    }
}
