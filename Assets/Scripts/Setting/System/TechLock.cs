using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TechLock : MonoBehaviour
{
    [SerializeField]
    int LockLevel;
    void Update()
    {
        if(LongPressEffect.nowCastleLevel >= LockLevel)
        {
            gameObject.SetActive(false);
        }
    }
}
