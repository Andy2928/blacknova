using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music_Wait : MonoBehaviour
{
    [SerializeField]
    float waitTimeSet;
    float timeCount;
    [SerializeField]
    GameObject music_After;

    // Update is called once per frame
    void Update()
    {
        timeCount += Time.deltaTime;
        if (timeCount >= waitTimeSet)
        {
            music_After.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}
