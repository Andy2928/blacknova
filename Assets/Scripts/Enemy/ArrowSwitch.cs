
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowSwitch : MonoBehaviour
{
    SpriteRenderer i_Object;
    bool b_Plus;
    void Start()
    {
        i_Object = gameObject.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (b_Plus == false)
        {
            i_Object.color = new Color(i_Object.color.r, i_Object.color.g, i_Object.color.b, i_Object.color.a - 0.1f * Time.deltaTime * 30);
            if (i_Object.color.a <= 0)
            {
                b_Plus = true;
            }
        }
        if (b_Plus == true)
        {
            i_Object.color = new Color(i_Object.color.r, i_Object.color.g, i_Object.color.b, i_Object.color.a + 0.1f * Time.deltaTime * 30);
            if (i_Object.color.a >= 1)
            {
                b_Plus = false;
            }
        }
    }
}

