using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBoomCollider : MonoBehaviour
{
    [SerializeField]
    GameObject m_Parent;
    [SerializeField]
    GameObject m_DeadIdol;
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Tower" && other.GetComponent<TowerSetting>().buildSet == true || other.tag == "Castle")
        {
            Instantiate(m_DeadIdol, transform.position, m_DeadIdol.transform.rotation);
            Destroy(m_Parent);
        }
    }
}
