using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    [SerializeField]
    [Header("1�d��A2����")]
    float enemyType;

    [SerializeField]
    GameObject m_Attack;
    [SerializeField]
    GameObject EnemyColi;
    [SerializeField]
    float add_X;
    [SerializeField]
    float add_Y;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }
    public void InsBullet()
    {
        if (enemyType == 1)
        {
            m_Attack.SetActive(true);
        }

        else if (enemyType == 2)
        {
            GameObject bullet = Instantiate(m_Attack, transform.parent.position + Vector3.forward * add_Y + Vector3.right * add_X, transform.rotation);
            bullet.GetComponent<SingleAttackCtrl>().SetTarget(EnemyColi.GetComponent<EnemyCollider>().m_Target.transform);
        }
    }
}
