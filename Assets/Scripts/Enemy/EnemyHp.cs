using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHp : MonoBehaviour
{

    public float hp = 2;
    [SerializeField]
    float maxHp = 2;

    [Header("護甲值")]
    public float armorValue;

    [SerializeField]
    GameObject ui_HpSlider;
    [SerializeField]
    Image hpSlider;

    [Header("受擊")]
    public bool b_OnAttacked;
    float onAttackedTimeCount;
    public GameObject attacked_Effect;

    [SerializeField]
    SpriteRenderer enemy_Skin;

    [SerializeField] 
    GameObject m_DeadIdol;

    [SerializeField]
    bool b_Dizzy;
    [SerializeField]
    float dizzy_Count;
    [SerializeField]
    Animator a_skin;
    [SerializeField]
    EnemyCollider e_Colli;
    [SerializeField]
    EnemyMove e_Move;

    //動畫速度
    float a_speed = 1;


    // Update is called once per frame
    void Update()
    {
        if (b_Dizzy == true)
        {
            a_skin.speed = 0;
            e_Colli.canAttack = false;
            e_Move.canMove = false;

            if (dizzy_Count <= 1)
            {
                dizzy_Count += Time.deltaTime;
            }
            if (dizzy_Count >= 1)
            {
                a_skin.speed = 1;
                e_Colli.canAttack = true;
                e_Move.canMove = true;
                dizzy_Count = 0;
                b_Dizzy = false;
            }
        }

        HPSliderCtrl();
        OnAttacked();
    }

    void OnAttacked()
    {
        if (b_OnAttacked == false)
        {
            onAttackedTimeCount = 0;
            enemy_Skin.color = Color.white;
        }
        if (b_OnAttacked == true)
        {
            onAttackedTimeCount += Time.deltaTime;
            if (onAttackedTimeCount <= 0.1f)
            {
                enemy_Skin.color = Color.red;
            }
            if (onAttackedTimeCount > 0.1f)
            {
                enemy_Skin.color = Color.white;
                b_OnAttacked = false;
            }
        }
    }
    void HPSliderCtrl()
    {
        hpSlider.fillAmount = hp / maxHp;


        if (hpSlider.fillAmount < 1)
        {
            ui_HpSlider.SetActive(true);
        }
        else
        {
            ui_HpSlider.SetActive(false);
        }

        if (hp <= 0)
        {
            //if (attacked_Effect != null)
            //{
            //    Instantiate(attacked_Effect, transform.position, attacked_Effect.transform.rotation);
            //}
            Instantiate(m_DeadIdol, gameObject.transform.position, m_DeadIdol.transform.rotation);
            Destroy(transform.parent.gameObject) ;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "ShotgunAtk")
        {
            if (attacked_Effect != null)
            {
                Instantiate(attacked_Effect, transform.position, attacked_Effect.transform.rotation);
            }

            if ((30 - armorValue) > 0)
                hp -= (30 - armorValue);
            else
            { }

            b_OnAttacked = false;
            b_OnAttacked = true;
        }
        if (other.tag == "XiaLeiAtk")
        {
            if (attacked_Effect != null)
            {
                Instantiate(attacked_Effect, transform.position, attacked_Effect.transform.rotation);
            }

            if (WorldControler.ShalehLV_9 == true && (100 - armorValue) > 0)
                hp -= (100 - armorValue);
            if (WorldControler.ShalehLV_8 == true && (80 - armorValue) > 0) 
                hp -= (80 - armorValue);
            if (WorldControler.ShalehLV_7 == true && (65 - armorValue) > 0)
                hp -= (65 - armorValue);
            if (WorldControler.ShalehLV_6 == true && (55 - armorValue) > 0)
                hp -= (55 - armorValue);
            if (WorldControler.ShalehLV_5 == true && (45 - armorValue) > 0) 
                hp -= (45 - armorValue);
            if (WorldControler.ShalehLV_4 == true && (40 - armorValue) > 0) 
                hp -= (40 - armorValue);
            if (WorldControler.ShalehLV_3 == true && (35 - armorValue) > 0) 
                hp -= (35 - armorValue);
            if (WorldControler.ShalehLV_2 == true && (30 - armorValue) > 0) 
                hp -= (30 - armorValue);
            if (WorldControler.ShalehLV_2 == false && (25 - armorValue) > 0) 
                hp -= (25 - armorValue);
            else
            { }

            if (hp <= 0)
            {
                WorldControler.ShalehExp += 1;
            }
            b_OnAttacked = false;
            b_OnAttacked = true;
        }
        if (other.tag == "FireAtk")
        {
            if (attacked_Effect != null)
            {
                Instantiate(attacked_Effect, transform.position, attacked_Effect.transform.rotation);
            }

            if ((20 - armorValue) > 0) 
            {
                hp -= (20 - armorValue);
            }
            else
            { }

            b_OnAttacked = false;
            b_OnAttacked = true;
        }
        if (other.tag == "BoomAtk")
        {
            if (attacked_Effect != null)
            {
                Instantiate(attacked_Effect, transform.position, attacked_Effect.transform.rotation);
            }

            if ((60 - armorValue) > 0)
                hp -= (60 - armorValue);
            else
            { }

            b_OnAttacked = false;
            b_OnAttacked = true;
        }
        if (other.tag == "LimeyAtk")
        {

            if (attacked_Effect != null)
            {
                Instantiate(attacked_Effect, transform.position, attacked_Effect.transform.rotation);
            }

            if (WorldControler.LimeyLV_8 == true && (285 - armorValue) > 0)
                hp -= (285 - armorValue);
            else if (WorldControler.LimeyLV_7 == true && (210 - armorValue) > 0)
                hp -= (210 - armorValue);
            else if (WorldControler.LimeyLV_5 == true && (160 - armorValue) > 0)
                hp -= (160 - armorValue);
            else if (WorldControler.LimeyLV_4 == true && (110 - armorValue) > 0)
                hp -= (110 - armorValue);
            else if (WorldControler.LimeyLV_2 == true && (85 - armorValue) > 0)
                hp -= (85 - armorValue);
            else if (WorldControler.LimeyLV_2 == false &&(60 - armorValue) > 0)
                hp -= (60 - armorValue);
            else
            { }

            b_OnAttacked = false;
            b_OnAttacked = true;
        }
        if (other.tag == "ElectricAtk")
        {
            if (attacked_Effect != null)
            {
                Instantiate(attacked_Effect, transform.position, attacked_Effect.transform.rotation);
            }

            if ((40 - armorValue) > 0)
                hp -= (40 - armorValue);
            else
            { }

            dizzy_Count = 0;
            b_Dizzy = true;

            b_OnAttacked = false;
            b_OnAttacked = true;
        }
        if (other.tag == "MagicBoomAtk")
        {
            if (attacked_Effect != null)
            {
                Instantiate(attacked_Effect, transform.position, attacked_Effect.transform.rotation);
            }

            hp -= 75;

            b_OnAttacked = false;
            b_OnAttacked = true;
        }
    }
}
