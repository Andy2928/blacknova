using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{
    [SerializeField]
    GameObject m_Skin;
    [SerializeField]
    GameObject m_Collider;
    [SerializeField]
    GameObject m_Attack;
    [SerializeField]
    float speed = 1;
    float basicSpeed = 0.9f;
    public Transform[] corners;
    int index = 0;
    float localScale_x;

    public bool canMove = true;


    void Start()
    {
        localScale_x = m_Skin.transform.localScale.x;
    }

    // Update is called once per frame
    void Update()
    {
        if (canMove == true)
        {
            Move();
        }
    }
    void Move()
    {
        if (m_Collider.GetComponent<EnemyCollider>().isFight == false)
        {
            if (index > corners.Length - 1)
            {
                return;
            }

            transform.Translate((corners[index].position - transform.position).normalized * Time.deltaTime * basicSpeed * speed);

            //����I���ش¦V
            Vector3 direction = m_Collider.transform.position - corners[index].transform.position;
            float angle = Mathf.Atan2(direction.z, direction.x) * Mathf.Rad2Deg;
            m_Collider.transform.rotation = Quaternion.AngleAxis(angle, -Vector3.up);
            m_Attack.transform.rotation = m_Collider.transform.rotation;
            if (corners[index].transform.position.x - transform.position.x > .2f)
            {
                m_Skin.transform.localScale = new Vector3 (localScale_x, m_Skin.transform.localScale.y, m_Skin.transform.localScale.z);
            }
            if (corners[index].transform.position.x - transform.position.x < -.2f)
            {
                m_Skin.transform.localScale = new Vector3 (-localScale_x, m_Skin.transform.localScale.y, m_Skin.transform.localScale.z);
            }
            if (Vector3.Distance(corners[index].position, transform.position) < 0.085f)
            {
                index++;
            }
        }
    }
}
