using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCollider : MonoBehaviour
{
    [Header("是否為迅蜂")]
    [SerializeField]
    bool isBee;
    [SerializeField]
    Animator a_Enemy;
    [SerializeField]
    GameObject m_Attack;
    public bool isFight;
    public GameObject m_Target;
    [Header("攻擊冷卻時間")]
    [SerializeField]
    float attackCold;
    float attackCount;

    public bool canAttack = true;


    private void Update()
    {
        if (canAttack == true)
        {
            Attack();
            TargetRefresh();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Tower" && other.GetComponent<TowerSetting>().buildSet == true || other.tag == "Castle")
        {
            m_Target = other.gameObject;
            isFight = true;
        }
    }

    void TargetRefresh()
    {
        if (m_Target != null)
        {
            if (isBee == true)
            {
                if (m_Target.tag == "Castle")
                {
                    if (m_Target.GetComponent<CastleControler>().hp <= 0)
                    {
                        m_Target = null;
                    }
                }
            }
            else
            {
                if (m_Target.tag == "Tower")
                {
                    if (m_Target.GetComponent<TowerSetting>().hp <= 0)
                    {
                        m_Target = null;
                    }
                }
                else if (m_Target.tag == "Castle")
                {
                    if (m_Target.GetComponent<CastleControler>().hp <= 0)
                    {
                        m_Target = null;
                    }
                }
            }
        }
        if (m_Target == null)
        {
            isFight = false;
        }
    }

    void Attack()
    {
        attackCount += Time.deltaTime;
        if (isFight == true)
        {
            a_Enemy.SetBool("Attack", true);
            if (attackCount >= attackCold)
            {
                attackCount = 0;
            }
        }

        if (m_Target == null)
        {
            a_Enemy.SetBool("Attack", false);
            attackCount = 0;
        }
    }
}