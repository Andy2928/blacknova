using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowMove : MonoBehaviour
{
    [SerializeField]
    GameObject m_Arrow;
    [SerializeField]
    GameObject m_Arrow_1;
    [SerializeField]
    float speed = 1;
    float basicSpeed = 0.9f;
    public Transform[] corners;
    int index = 0;

    // Update is called once per frame
    void Update()
    {
        Move();
    }
    void Move()
    {
        if (index > corners.Length - 1)
        {
            Destroy(gameObject);
            return;
        }

        transform.Translate((corners[index].position - transform.position).normalized * Time.deltaTime * basicSpeed * speed);

        //����I���ش¦V
        Vector3 direction = m_Arrow.transform.position - corners[index].transform.position;
        float angle = Mathf.Atan2(direction.z, direction.x) * Mathf.Rad2Deg;
        m_Arrow.transform.rotation = Quaternion.AngleAxis(angle, -Vector3.up);
        m_Arrow_1.transform.rotation = Quaternion.AngleAxis(angle, -Vector3.up);

        if (Vector3.Distance(corners[index].position, transform.position) < 0.085f)
        {
            index++;
        }
    }
}
