using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ImmediateDialog : MonoBehaviour
{
    [Header("�C�y��ܫ���ɶ�")]
    [SerializeField]
    float textTimeSet;
    float textTimeCount;

    [SerializeField]
    TMP_Text t_Title;
    [SerializeField]
    TMP_Text t_Content;
    [SerializeField]
    Image i_face;


    [SerializeField]
    int index;

    [SerializeField]
    Sprite[] face;

    public TextAsset[] textFile;

    List<string> textList = new List<string>();
    [Header("�׭^bool")]
    public bool eliteTalking;
    public bool xialei_In;
    public bool xialei_Out;
    public bool limey_In;
    public bool limey_Out;
    public bool safente_In;
    public bool safente_Out;

    [Header("�Ĥ@���о�")]
    [SerializeField]
    GameObject m_DefenseTeach;
    [SerializeField]
    GameObject m_EliteTeach;
    [Header("�D���о�")]
    [SerializeField]
    GameObject m_TechtreeTeach;

    [SerializeField]
    GameObject unimask;
    private void Start()
    {
        
    }
    private void OnEnable()
    {
        DialogChooseSet();
        SetTextUI();
    }

    // Update is called once per frame
    void Update()
    {
        textTimeCount += Time.deltaTime;
        if (textTimeCount >= textTimeSet)
        {
            if (index == textList.Count)
            {
                gameObject.SetActive(false);
                index = 0;
                textTimeCount = 0;
                Teach();
                return;
            }

            SetTextUI();
            textTimeCount = 0;
        }
    }

    void SetTextUI()
    {
        switch (textList[index].Trim().ToString())
        {
            //�Ȼ�
            case "ArchSmile":
                t_Title.text = "�Ȼ�";
                i_face.sprite = face[0];
                index++;
                break;
            case "ArchSerious":
                t_Title.text = "�Ȼ�";
                i_face.sprite = face[1];
                index++;
                break;
            case "ArchClose":
                t_Title.text = "�Ȼ�";
                i_face.sprite = face[2];
                index++;
                break;
            case "ArchLaugh":
                t_Title.text = "�Ȼ�";
                i_face.sprite = face[3];
                index++;
                break;
            case "ArchEvil":
                t_Title.text = "�Ȼ�";
                i_face.sprite = face[4];
                index++;
                break;
            case "ArchSmug":
                t_Title.text = "�Ȼ�";
                i_face.sprite = face[5];
                index++;
                break;
            case "ArchHate":
                t_Title.text = "�Ȼ�";
                i_face.sprite = face[6];
                index++;
                break;

            //�L��
            case "ShalehSerious":
                t_Title.text = "�L��";
                i_face.sprite = face[7];
                index++;
                break;
            case "ShalehWorry":
                t_Title.text = "�L��";
                i_face.sprite = face[8];
                index++;
                break;
            case "ShalehPanic":
                t_Title.text = "�L��";
                i_face.sprite = face[9];
                index++;
                break;
            case "ShalehWorship":
                t_Title.text = "�L��";
                i_face.sprite = face[10];
                index++;
                break;
            case "ShalehSmile":
                t_Title.text = "�L��";
                i_face.sprite = face[11];
                index++;
                break;
            case "ShalehHappy":
                t_Title.text = "�L��";
                i_face.sprite = face[12];
                index++;
                break;
            case "ShalehConfuse":
                t_Title.text = "�L��";
                i_face.sprite = face[13];
                index++;
                break;

            //�R����
            case "NamelSmile":
                t_Title.text = "�R����";
                i_face.sprite = face[14];
                index++;
                break;
            case "NamelAnxious":
                t_Title.text = "�R����";
                i_face.sprite = face[15];
                index++;
                break;
            case "NamelPanic":
                t_Title.text = "�R����";
                i_face.sprite = face[16];
                index++;
                break;
            case "NamelAngry":
                t_Title.text = "�R����";
                i_face.sprite = face[17];
                index++;
                break;
            case "NamelEvil":
                t_Title.text = "�R����";
                i_face.sprite = face[18];
                index++;
                break;
            case "NamelHappy":
                t_Title.text = "�R����";
                i_face.sprite = face[19];
                index++;
                break;

            //�ܫ}
            case "LimeySmile":
                t_Title.text = "�ܫ}";
                i_face.sprite = face[20];
                index++;
                break;
            case "LimeyEvil":
                t_Title.text = "�ܫ}";
                i_face.sprite = face[21];
                index++;
                break;
            case "LimeySurprise":
                t_Title.text = "�ܫ}";
                i_face.sprite = face[22];
                index++;
                break;
            case "LimeyCalm":
                t_Title.text = "�ܫ}";
                i_face.sprite = face[23];
                index++;
                break;
            case "LimeyEcstasy":
                t_Title.text = "�ܫ}";
                i_face.sprite = face[24];
                index++;
                break;

            //�d
            case "ShaoSmile":
                t_Title.text = "�d";
                i_face.sprite = face[25];
                index++;
                break;
            case "ShaoConfuse":
                t_Title.text = "�d";
                i_face.sprite = face[26];
                index++;
                break;
            case "ShaoSpeechless":
                t_Title.text = "�d";
                i_face.sprite = face[27];
                index++;
                break;
            case "ShaoHappy":
                t_Title.text = "�d";
                i_face.sprite = face[28];
                index++;
                break;
            case "ShaoSad":
                t_Title.text = "�d";
                i_face.sprite = face[29];
                index++;
                break;

            //����
            case "LortaSpeechless":
                t_Title.text = "����";
                i_face.sprite = face[30];
                index++;
                break;
            case "LortaAngry":
                t_Title.text = "����";
                i_face.sprite = face[31];
                index++;
                break;
            case "LortaTear":
                t_Title.text = "����";
                i_face.sprite = face[32];
                index++;
                break;
            case "LortaCry":
                t_Title.text = "����";
                i_face.sprite = face[33];
                index++;
                break;
            case "LortaSurprise":
                t_Title.text = "����";
                i_face.sprite = face[34];
                index++;
                break;
            case "LortaSmug":
                t_Title.text = "����";
                i_face.sprite = face[35];
                index++;
                break;
            case "LortaSmile":
                t_Title.text = "����";
                i_face.sprite = face[36];
                index++;
                break;

            //���S
            case "SerphentCalm":
                t_Title.text = "���S";
                i_face.sprite = face[37];
                index++;
                break;
            case "SerphentClose":
                t_Title.text = "���S";
                i_face.sprite = face[38];
                index++;
                break;
            case "SerphentSurprise":
                t_Title.text = "���S";
                i_face.sprite = face[39];
                index++;
                break;
            case "SerphentUpset":
                t_Title.text = "���S";
                i_face.sprite = face[40];
                index++;
                break;
            case "SerphentAngry":
                t_Title.text = "���S";
                i_face.sprite = face[41];
                index++;
                break;
            case "SerphentSmile":
                t_Title.text = "���S";
                i_face.sprite = face[42];
                index++;
                break;

            //���S�S
            case "IluluWork":
                t_Title.text = "���S�S";
                i_face.sprite = face[43];
                index++;
                break;
            case "IluluGlare":
                t_Title.text = "���S�S";
                i_face.sprite = face[44];
                index++;
                break;
            case "IluluAngry":
                t_Title.text = "���S�S";
                i_face.sprite = face[45];
                index++;
                break;
            case "IluluClose":
                t_Title.text = "���S�S";
                i_face.sprite = face[46];
                index++;
                break;
            case "IluluBlush":
                t_Title.text = "���S�S";
                i_face.sprite = face[47];
                index++;
                break;
            case "IluluPanic":
                t_Title.text = "���S�S";
                i_face.sprite = face[48];
                index++;
                break;
        }
        t_Content.text = textList[index];
        index++;
    }
    void GetTextFromFile(TextAsset file)
    {
        textList.Clear();
        index = 0;

        var lineData = file.text.Split('\n');

        foreach (var line in lineData)
        {
            textList.Add(line);
        }
    }
    //��ܨϥέ��y���
    void DialogChooseSet()
    {
        if(eliteTalking == true)
        {
            int i = Random.Range(0, 3);

            if (xialei_In == true)
            {
                if (i == 0)
                {
                    GetTextFromFile(textFile[5]);
                }
                if(i == 1)
                {
                    GetTextFromFile(textFile[6]);
                }
                if(i == 2)
                {
                    GetTextFromFile(textFile[7]);
                }
                xialei_In = false;
            }
            if (xialei_Out == true)
            {
                if (i == 0)
                {
                    GetTextFromFile(textFile[8]);
                }
                if (i == 1)
                {
                    GetTextFromFile(textFile[9]);
                }
                if (i == 2)
                {
                    GetTextFromFile(textFile[10]);
                }
                xialei_Out = false;
            }

            if (limey_In == true)
            {
                if (i == 0)
                {
                    GetTextFromFile(textFile[13]);
                }
                if (i == 1)
                {
                    GetTextFromFile(textFile[14]);
                }
                if (i == 2)
                {
                    GetTextFromFile(textFile[15]);
                }
                limey_In = false;
            }
            if (limey_Out == true)
            {
                if (i == 0)
                {
                    GetTextFromFile(textFile[16]);
                }
                if (i == 1)
                {
                    GetTextFromFile(textFile[17]);
                }
                if (i == 2)
                {
                    GetTextFromFile(textFile[18]);
                }
                limey_Out = false;
            }

            if (safente_In == true)
            {
                if (i == 0)
                {
                    GetTextFromFile(textFile[21]);
                }
                if (i == 1)
                {
                    GetTextFromFile(textFile[22]);
                }
                if (i == 2)
                {
                    GetTextFromFile(textFile[23]);
                }
                safente_In = false;
            }
            if (safente_Out == true)
            {
                if (i == 0)
                {
                    GetTextFromFile(textFile[24]);
                }
                if (i == 1)
                {
                    GetTextFromFile(textFile[25]);
                }
                if (i == 2)
                {
                    GetTextFromFile(textFile[26]);
                }
                safente_Out = false;
            }

            eliteTalking = false;
        }
        else if (eliteTalking == false)
        {
            if (WorldControler.teachPass_One == false)
            {
                TeachTalk1();
            }
            TeachTalkMenu();
            if (WorldControler.pass_Two == false)
            {
                TeachTalk2();
            }
            if (WorldControler.pass_Three == false)
            {
                TeachTalk3();
            }
            if (WorldControler.pass_Four == false)
            {
                TeachTalk4();
            }
        }

        textTimeCount = 0;
        index = 0;
    }
    void TeachTalkMenu()
    {
        if (WorldControler.isTalk6 == true)
        {
            GetTextFromFile(textFile[11]);
            return;
        }
    }
    void TeachTalk1()
    {
        if (WorldControler.isTalk5 == true)
        {
            GetTextFromFile(textFile[4]);
            return;
        }
        else if (WorldControler.isTalk4 == true)
        {
            unimask.SetActive(true);
            GetTextFromFile(textFile[3]);
            return;
        }
        else if (WorldControler.outpostBuiled == true)
        {
            GetTextFromFile(textFile[2]);
            return;
        }
        else if (WorldControler.houseBuildTeach == true)
        {
            GetTextFromFile(textFile[1]);
            return;
        }
        else if (WorldControler.workerBuildTeach == true)
        {
            GetTextFromFile(textFile[0]);
            return;
        }
    }
    void TeachTalk2()
    {
        if (WorldControler.b_levelTwoStart == true)
        {
            GetTextFromFile(textFile[12]);
            return;
        }
    }
    void TeachTalk3()
    {
        if (WorldControler.b_levelThreeStart == true)
        {
            GetTextFromFile(textFile[19]);
            return;
        }
    }
    void TeachTalk4()
    {
        if (WorldControler.b_levelFourStart == true)
        {
            GetTextFromFile(textFile[20]);
            return;
        }
    }
    void Teach()
    {
        if (WorldControler.outpostBuiled == true && WorldControler.defenseTeach == false)
        {
            WorldControler.defenseTeach = true;
            m_DefenseTeach.SetActive(true);
        }
        if(WorldControler.isTalk5 == true && WorldControler.eliteTeach == false)
        {
            WorldControler.eliteTeach = true;
            m_EliteTeach.SetActive(true);
        }
        //if (WorldControler.isTalk6 == true && WorldControler.teachTechTree == false)
        //{
        //    WorldControler.teachTechTree = true;
        //    m_TechtreeTeach.SetActive(true);
        //}
    }
}
