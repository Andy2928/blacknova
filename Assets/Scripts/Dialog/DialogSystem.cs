using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogSystem : MonoBehaviour
{
    public bool b_Switch = false;
    float switchTime;

    [SerializeField]
    TMP_Text t_Title;
    [SerializeField]
    TMP_Text t_Content;
    [SerializeField]
    Image i_face;

    
    [SerializeField]
    int index;
    [SerializeField]
    float textSpeed;

    bool textFinished;
    bool cancelTyping;
    public static bool isTalking;

    [SerializeField]
    Sprite[] face;

    //���d�@���]�w
    [SerializeField]
    int thisLevel;
    [SerializeField]
    TextAsset[] textFile;
    [Header("����")]
    [SerializeField]
    GameObject m_WorkerTeach;
    [SerializeField]
    GameObject m_Transition;

    List<string> textList = new List<string>();
    private void OnEnable()
    {
        if (thisLevel == 0 && WorldControler.teachPass_One == true)
        {
            gameObject.SetActive(false);
        }
        if (thisLevel == 2 && WorldControler.pass_Two == true)
        {
            gameObject.SetActive(false);
        }
        else
        {
            isTalking = true;
            DialogLevelSet();
            StartCoroutine(SetTextUI());
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (textFinished == true && Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (index == textList.Count)
            {
                index = 0;
                isTalking = false;
                Teach();
                DialogEvent();

                gameObject.SetActive(false);
                return;
            }

            StartCoroutine(SetTextUI());
            isTalking = true;
        }
        else if (textFinished == false && Input.GetKeyDown(KeyCode.Mouse0))
        {
            cancelTyping = true;
        }
        SwitchHead();
    }
    void SwitchHead()
    {
        if (b_Switch == true)
        {
            i_face.transform.localPosition = Vector3.Lerp(i_face.transform.localPosition, new Vector3(-1200, 203, 0), 0.9f);
            switchTime += Time.deltaTime;
            if (switchTime >= 0.1f)
            {
                b_Switch = false;
                switchTime = 0;
            }
        }
        if (b_Switch == false)
        {
            i_face.transform.localPosition = Vector3.Lerp(i_face.transform.localPosition, new Vector3(-609, 203, 0), 0.5f);
        }
    }
    void GetTextFromFile(TextAsset file)
    {
        textList.Clear();
        index = 0;

         var lineData = file.text.Split('\n');

        foreach (var line in lineData)
        {
            textList.Add(line);
        }
    }

    IEnumerator SetTextUI()
    {
        textFinished = false;
        t_Content.text = "";

        switch(textList[index].Trim().ToString())
        {
            //�Ȼ�
            case "ArchSmile_Switch":
                t_Title.text = "�Ȼ�";//�����W��
                b_Switch = true;//�X��J��
                i_face.sprite = face[0];//�����Y��
                index++;
                break;
            case "ArchSerious_Switch":
                t_Title.text = "�Ȼ�";
                b_Switch = true;
                i_face.sprite = face[1];
                index++;
                break;
            case "ArchClose_Switch":
                t_Title.text = "�Ȼ�";
                b_Switch = true;
                i_face.sprite = face[2];
                index++;
                break;
            case "ArchLaugh_Switch":
                t_Title.text = "�Ȼ�";
                b_Switch = true;
                i_face.sprite = face[3];
                index++;
                break;
            case "ArchEvil_Switch":
                t_Title.text = "�Ȼ�";
                b_Switch = true;
                i_face.sprite = face[4];
                index++;
                break;
            case "ArchSmug_Switch":
                t_Title.text = "�Ȼ�";
                b_Switch = true;
                i_face.sprite = face[5];
                index++;
                break;
            case "ArchHate_Switch":
                t_Title.text = "�Ȼ�";
                b_Switch = true;
                i_face.sprite = face[6];
                index++;
                break;
            case "ArchSmile":
                t_Title.text = "�Ȼ�";
                i_face.sprite = face[0];
                index++;
                break;
            case "ArchSerious":
                t_Title.text = "�Ȼ�";
                i_face.sprite = face[1];
                index++;
                break;
            case "ArchClose":
                t_Title.text = "�Ȼ�";
                i_face.sprite = face[2];
                index++;
                break;
            case "ArchLaugh":
                t_Title.text = "�Ȼ�";
                i_face.sprite = face[3];
                index++;
                break;
            case "ArchEvil":
                t_Title.text = "�Ȼ�";
                i_face.sprite = face[4];
                index++;
                break;
            case "ArchSmug":
                t_Title.text = "�Ȼ�";
                i_face.sprite = face[5];
                index++;
                break;
            case "ArchHate":
                t_Title.text = "�Ȼ�";
                i_face.sprite = face[6];
                index++;
                break;

            //�L��
            case "ShalehSerious_Switch":
                t_Title.text = "�L��";
                b_Switch = true;
                i_face.sprite = face[7];
                index++;
                break;
            case "ShalehWorry_Switch":
                t_Title.text = "�L��";
                b_Switch = true;
                i_face.sprite = face[8];
                index++;
                break;
            case "ShalehPanic_Switch":
                t_Title.text = "�L��";
                b_Switch = true;
                i_face.sprite = face[9];
                index++;
                break;
            case "ShalehWorship_Switch":
                t_Title.text = "�L��";
                b_Switch = true;
                i_face.sprite = face[10];
                index++;
                break;
            case "ShalehSmile_Switch":
                t_Title.text = "�L��";
                b_Switch = true;
                i_face.sprite = face[11];
                index++;
                break;
            case "ShalehHappy_Switch":
                t_Title.text = "�L��";
                b_Switch = true;
                i_face.sprite = face[12];
                index++;
                break;
            case "ShalehConfuse_Switch":
                t_Title.text = "�L��";
                b_Switch = true;
                i_face.sprite = face[13];
                index++;
                break;
            case "ShalehSerious":
                t_Title.text = "�L��";
                i_face.sprite = face[7];
                index++;
                break;
            case "ShalehWorry":
                t_Title.text = "�L��";
                i_face.sprite = face[8];
                index++;
                break;
            case "ShalehPanic":
                t_Title.text = "�L��";
                i_face.sprite = face[9];
                index++;
                break;
            case "ShalehWorship":
                t_Title.text = "�L��";
                i_face.sprite = face[10];
                index++;
                break;
            case "ShalehSmile":
                t_Title.text = "�L��";
                i_face.sprite = face[11];
                index++;
                break;
            case "ShalehHappy":
                t_Title.text = "�L��";
                i_face.sprite = face[12];
                index++;
                break;
            case "ShalehConfuse":
                t_Title.text = "�L��";
                i_face.sprite = face[13];
                index++;
                break;

            //�R����
            case "NamelSmile_Switch":
                t_Title.text = "�R����";
                b_Switch = true;
                i_face.sprite = face[14];
                index++;
                break;
            case "NamelAnxious_Switch":
                t_Title.text = "�R����";
                b_Switch = true;
                i_face.sprite = face[15];
                index++;
                break;
            case "NamelPanic_Switch":
                t_Title.text = "�R����";
                b_Switch = true;
                i_face.sprite = face[16];
                index++;
                break;
            case "NamelAngry_Switch":
                t_Title.text = "�R����";
                b_Switch = true;
                i_face.sprite = face[17];
                index++;
                break;
            case "NamelEvil_Switch":
                t_Title.text = "�R����";
                b_Switch = true;
                i_face.sprite = face[18];
                index++;
                break;
            case "NamelHappy_Switch":
                t_Title.text = "�R����";
                b_Switch = true;
                i_face.sprite = face[19];
                index++;
                break;
            case "NamelSmile":
                t_Title.text = "�R����";
                i_face.sprite = face[14];
                index++;
                break;
            case "NamelAnxious":
                t_Title.text = "�R����";
                i_face.sprite = face[15];
                index++;
                break;
            case "NamelPanic":
                t_Title.text = "�R����";
                i_face.sprite = face[16];
                index++;
                break;
            case "NamelAngry":
                t_Title.text = "�R����";
                i_face.sprite = face[17];
                index++;
                break;
            case "NamelEvil":
                t_Title.text = "�R����";
                i_face.sprite = face[18];
                index++;
                break;
            case "NamelHappy":
                t_Title.text = "�R����";
                i_face.sprite = face[19];
                index++;
                break;

            //�ܫ}
            case "LimeySmile_Switch":
                t_Title.text = "�ܫ}";
                b_Switch = true;
                i_face.sprite = face[20];
                index++;
                break;
            case "LimeyEvil_Switch":
                t_Title.text = "�ܫ}";
                b_Switch = true;
                i_face.sprite = face[21];
                index++;
                break;
            case "LimeySurprise_Switch":
                t_Title.text = "�ܫ}";
                b_Switch = true;
                i_face.sprite = face[22];
                index++;
                break;
            case "LimeyCalm_Switch":
                t_Title.text = "�ܫ}";
                b_Switch = true;
                i_face.sprite = face[23];
                index++;
                break;
            case "LimeyEcstasy_Switch":
                t_Title.text = "�ܫ}";
                b_Switch = true;
                i_face.sprite = face[24];
                index++;
                break;
            case "LimeySmile":
                t_Title.text = "�ܫ}";
                i_face.sprite = face[20];
                index++;
                break;
            case "LimeyEvil":
                t_Title.text = "�ܫ}";
                i_face.sprite = face[21];
                index++;
                break;
            case "LimeySurprise":
                t_Title.text = "�ܫ}";
                i_face.sprite = face[22];
                index++;
                break;
            case "LimeyCalm":
                t_Title.text = "�ܫ}";
                i_face.sprite = face[23];
                index++;
                break;
            case "LimeyEcstasy":
                t_Title.text = "�ܫ}";
                i_face.sprite = face[24];
                index++;
                break;

            //�d
            case "ShaoSmile_Switch":
                t_Title.text = "�d";
                b_Switch = true;
                i_face.sprite = face[25];
                index++;
                break;
            case "ShaoConfuse_Switch":
                t_Title.text = "�d";
                b_Switch = true;
                i_face.sprite = face[26];
                index++;
                break;
            case "ShaoSpeechless_Switch":
                t_Title.text = "�d";
                b_Switch = true;
                i_face.sprite = face[27];
                index++;
                break;
            case "ShaoHappy_Switch":
                t_Title.text = "�d";
                b_Switch = true;
                i_face.sprite = face[28];
                index++;
                break;
            case "ShaoSad_Switch":
                t_Title.text = "�d";
                b_Switch = true;
                i_face.sprite = face[29];
                index++;
                break;
            case "ShaoSmile":
                t_Title.text = "�d";
                i_face.sprite = face[25];
                index++;
                break;
            case "ShaoConfuse":
                t_Title.text = "�d";
                i_face.sprite = face[26];
                index++;
                break;
            case "ShaoSpeechless":
                t_Title.text = "�d";
                i_face.sprite = face[27];
                index++;
                break;
            case "ShaoHappy":
                t_Title.text = "�d";
                i_face.sprite = face[28];
                index++;
                break;
            case "ShaoSad":
                t_Title.text = "�d";
                i_face.sprite = face[29];
                index++;
                break;

            //����
            case "LortaSpeechless_Switch":
                t_Title.text = "����";
                b_Switch = true;
                i_face.sprite = face[30];
                index++;
                break;
            case "LortaAngry_Switch":
                t_Title.text = "����";
                b_Switch = true;
                i_face.sprite = face[31];
                index++;
                break;
            case "LortaTear_Switch":
                t_Title.text = "����";
                b_Switch = true;
                i_face.sprite = face[32];
                index++;
                break;
            case "LortaCry_Switch":
                t_Title.text = "����";
                b_Switch = true;
                i_face.sprite = face[33];
                index++;
                break;
            case "LortaSurprise_Switch":
                t_Title.text = "����";
                b_Switch = true;
                i_face.sprite = face[34];
                index++;
                break;
            case "LortaSmug_Switch":
                t_Title.text = "����";
                b_Switch = true;
                i_face.sprite = face[35];
                index++;
                break;
            case "LortaSmile_Switch":
                t_Title.text = "����";
                b_Switch = true;
                i_face.sprite = face[36];
                index++;
                break;
            case "LortaSpeechless":
                t_Title.text = "����";
                i_face.sprite = face[30];
                index++;
                break;
            case "LortaAngry":
                t_Title.text = "����";
                i_face.sprite = face[31];
                index++;
                break;
            case "LortaTear":
                t_Title.text = "����";
                i_face.sprite = face[32];
                index++;
                break;
            case "LortaCry":
                t_Title.text = "����";
                i_face.sprite = face[33];
                index++;
                break;
            case "LortaSurprise":
                t_Title.text = "����";
                i_face.sprite = face[34];
                index++;
                break;
            case "LortaSmug":
                t_Title.text = "����";
                i_face.sprite = face[35];
                index++;
                break;
            case "LortaSmile":
                t_Title.text = "����";
                i_face.sprite = face[36];
                index++;
                break;

            //���S
            case "SerphentCalm_Switch":
                t_Title.text = "���S";
                b_Switch = true;
                i_face.sprite = face[37];
                index++;
                break;
            case "SerphentClose_Switch":
                t_Title.text = "���S";
                b_Switch = true;
                i_face.sprite = face[38];
                index++;
                break;
            case "SerphentSurprise_Switch":
                t_Title.text = "���S";
                b_Switch = true;
                i_face.sprite = face[39];
                index++;
                break;
            case "SerphentUpset_Switch":
                t_Title.text = "���S";
                b_Switch = true;
                i_face.sprite = face[40];
                index++;
                break;
            case "SerphentAngry_Switch":
                t_Title.text = "���S";
                b_Switch = true;
                i_face.sprite = face[41];
                index++;
                break;
            case "SerphentSmile_Switch":
                t_Title.text = "���S";
                b_Switch = true;
                i_face.sprite = face[42];
                index++;
                break;
            case "SerphentCalm":
                t_Title.text = "���S";
                i_face.sprite = face[37];
                index++;
                break;
            case "SerphentClose":
                t_Title.text = "���S";
                i_face.sprite = face[38];
                index++;
                break;
            case "SerphentSurprise":
                t_Title.text = "���S";
                i_face.sprite = face[39];
                index++;
                break;
            case "SerphentUpset":
                t_Title.text = "���S";
                i_face.sprite = face[40];
                index++;
                break;
            case "SerphentAngry":
                t_Title.text = "���S";
                i_face.sprite = face[41];
                index++;
                break;
            case "SerphentSmile":
                t_Title.text = "���S";
                i_face.sprite = face[42];
                index++;
                break;

            //���S�S
            case "IluluWork_Switch":
                t_Title.text = "���S�S";
                b_Switch = true;
                i_face.sprite = face[43];
                index++;
                break;
            case "IluluGlare_Switch":
                t_Title.text = "���S�S";
                b_Switch = true;
                i_face.sprite = face[44];
                index++;
                break;
            case "IluluAngry_Switch":
                t_Title.text = "���S�S";
                b_Switch = true;
                i_face.sprite = face[45];
                index++;
                break;
            case "IluluClose_Switch":
                t_Title.text = "���S�S";
                b_Switch = true;
                i_face.sprite = face[46];
                index++;
                break;
            case "IluluBlush_Switch":
                t_Title.text = "���S�S";
                b_Switch = true;
                i_face.sprite = face[47];
                index++;
                break;
            case "IluluPanic_Switch":
                t_Title.text = "���S�S";
                b_Switch = true;
                i_face.sprite = face[48];
                index++;
                break;
            case "IluluWork":
                t_Title.text = "���S�S";
                i_face.sprite = face[43];
                index++;
                break;
            case "IluluGlare":
                t_Title.text = "���S�S";
                i_face.sprite = face[44];
                index++;
                break;
            case "IluluAngry":
                t_Title.text = "���S�S";
                i_face.sprite = face[45];
                index++;
                break;
            case "IluluClose":
                t_Title.text = "���S�S";
                i_face.sprite = face[46];
                index++;
                break;
            case "IluluBlush":
                t_Title.text = "���S�S";
                i_face.sprite = face[47];
                index++;
                break;
            case "IluluPanic":
                t_Title.text = "���S�S";
                i_face.sprite = face[48];
                index++;
                break;
        }

        int letter = 0;
        while (!cancelTyping && letter < textList[index].Length - 1)
        {
            t_Content.text += textList[index][letter];
            letter++;
            yield return new WaitForSeconds(textSpeed);
        }

        t_Content.text = textList[index];
        cancelTyping = false;
        textFinished = true;
        index++;
    }
    void Teach()
    {
        if (thisLevel == 0 && WorldControler.isEnemyClear == false && WorldControler.teachPass_One == false)
        {
            WorldControler.crystalOutPut = 1;
            WorldControler.foodOutPut = 1;
            WorldControler.gasOutPut = 1;
            m_WorkerTeach.SetActive(true);
        }
    }
    void DialogEvent()
    {
        if (thisLevel == 1 && WorldControler.plot_2 == false)
        {
            WorldControler.plot_2 = true;
            m_Transition.SetActive(true);
        }
        else if (thisLevel == 1 && WorldControler.plot_3 == false)
        {
            MainMenuCtrl.openFunction = true;
            WorldControler.plot_3 = true;
            m_Transition.SetActive(true);
        }
        else if (thisLevel == 2 && WorldControler.b_levelTwoEnd == false)
        {
            WorldControler.b_levelTwoEnd = true;
        }
        else if (thisLevel == 1 && WorldControler.plot_4 == false && WorldControler.pass_Two)
        {
            WorldControler.plot_4 = true;
            m_Transition.SetActive(true);
        }
        else if (thisLevel == 1 && WorldControler.plot_5 == false && WorldControler.pass_Two)
        {
            WorldControler.plot_5 = true;
            m_Transition.SetActive(true);
        }
        else if (thisLevel == 1 && WorldControler.plot_6 == false && WorldControler.pass_Two)
        {
            MainMenuCtrl.openFunction = true;
            WorldControler.plot_6 = true;
            m_Transition.SetActive(true);
        }
        else if (thisLevel == 3 && WorldControler.b_levelThreeEnd == false)
        {
            WorldControler.b_levelThreeEnd = true;
        }
        else if (thisLevel == 4 && WorldControler.b_levelFourEnd == false)
        {
            WorldControler.b_levelFourEnd = true;
        }
        else if (thisLevel == 1 && WorldControler.plot_9 == false)
        {
            WorldControler.plot_9 = true;
            m_Transition.SetActive(true);
        }
        else if (thisLevel == 1 && WorldControler.plot_10 == false)
        {
            WorldControler.plot_10 = true;
            m_Transition.SetActive(true);
        }
        else if (thisLevel == 1 && WorldControler.plot_11 == false)
        {
            MainMenuCtrl.openFunction = true;
            WorldControler.plot_11 = true;
            m_Transition.SetActive(true);
        }
    }
    void DialogLevelSet()
    {
        if(WorldControler.isEnemyClear == false && thisLevel == 0)
        {
            GetTextFromFile(textFile[0]);
        }
        else if (thisLevel == 1 && WorldControler.plot_2 == false)
        {
            MainMenuCtrl.openFunction = false;
            GetTextFromFile(textFile[1]);
        }
        else if (thisLevel == 1 && WorldControler.plot_3 == false)
        {
            GetTextFromFile(textFile[2]);
        }
        else if (thisLevel == 2 && WorldControler.b_levelTwoEnd == false)
        {
            MainMenuCtrl.openFunction = false;
            GetTextFromFile(textFile[3]);
        }
        else if (thisLevel == 1 && WorldControler.plot_4 == false)
        {
            GetTextFromFile(textFile[4]);
        }
        else if (thisLevel == 1 && WorldControler.plot_5 == false)
        {
            GetTextFromFile(textFile[5]);
        }
        else if (thisLevel == 1 && WorldControler.plot_6 == false)
        {
            GetTextFromFile(textFile[6]);
        }
        else if (thisLevel == 3 && WorldControler.b_levelThreeEnd == false)
        {
            GetTextFromFile(textFile[7]);
        }
        else if (thisLevel == 4 && WorldControler.b_levelFourEnd == false)
        {
            MainMenuCtrl.openFunction = false;
            GetTextFromFile(textFile[8]);
        }
        else if (thisLevel == 1 && WorldControler.plot_9 == false)
        {
            GetTextFromFile(textFile[9]);
        }
        else if (thisLevel == 1 && WorldControler.plot_10 == false)
        {
            GetTextFromFile(textFile[10]);
        }
        else if (thisLevel == 1 && WorldControler.plot_11 == false)
        {
            GetTextFromFile(textFile[11]);
        }
        index = 0;
    }

    public void SkipDialog()
    {
        index = 0;
        isTalking = false;
        Teach();
        DialogEvent();

        gameObject.SetActive(false);
    }
}