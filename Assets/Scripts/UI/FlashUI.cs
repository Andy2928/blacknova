using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashUI : MonoBehaviour
{
    [Header("0消失，1破壞")]
    [SerializeField]
    float type;
    float timeCount;
    [SerializeField]
    [Header("UI持續時間")]
    float timeSet;

    void Update()
    {
        timeCount += Time.deltaTime;
        if(timeCount >= timeSet)
        {
            timeCount = 0;
            if (type == 0)
                gameObject.SetActive(false);
            if (type == 1)
                Destroy(gameObject);
        }
    }
}
