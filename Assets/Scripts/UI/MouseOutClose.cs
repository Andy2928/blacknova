using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseOutClose : MonoBehaviour
{
    [SerializeField]
    GameObject originBtn;

    private void Update()
    {
        bool mouseOnBtn = RectTransformUtility.RectangleContainsScreenPoint(gameObject.GetComponent<Image>().rectTransform, Input.mousePosition);
        if (mouseOnBtn == false)
        {
            originBtn.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}
