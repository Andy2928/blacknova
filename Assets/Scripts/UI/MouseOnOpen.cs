using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseOnOpen : MonoBehaviour
{
    [SerializeField]
    GameObject selectBtn;
    private void Update()
    {
        bool mouseOnBtn = RectTransformUtility.RectangleContainsScreenPoint(gameObject.GetComponent<Image>().rectTransform, Input.mousePosition);
        if (mouseOnBtn == true)
        {
            selectBtn.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}
