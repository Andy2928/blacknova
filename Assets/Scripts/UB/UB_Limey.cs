using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UB_Limey : MonoBehaviour
{
    [SerializeField]
    Image hpSlider;
    [SerializeField]
    Image pictureHpSlider;

    [Header("奧義鈕")]
    [SerializeField]
    Button ubBtn;
    [Header("奧義冷卻")]
    [SerializeField]
    float ubCdTimeSet;
    float ubCdTimeCount;

    [Header("復活圖示")]
    [SerializeField]
    Image reviveImg;

    [Header("攻擊")]
    [SerializeField]
    GameObject attack;
    [Header("爆炸範圍")]
    [SerializeField]
    GameObject range;

    [Header("復活時間")]
    [SerializeField]
    float cdTimeSet;
    [SerializeField]
    float cdTimeSet_Basic;
    [SerializeField]
    float cdTimeCount;

    [SerializeField]
    GameObject m_Idle;
    [SerializeField]
    GameObject tower_Depoly;
    [SerializeField]
    GameObject RemoveBtn;
    [SerializeField]
    GameObject towerPicture;
    [SerializeField]
    GameObject selectFrame;
    [SerializeField]
    GameObject selectedFrame;
    [SerializeField]
    GameObject TowerRange_UI;

    //在不可建造的區域上(陰影中)
    bool inShadow = false;

    private void OnEnable()
    {
        ubBtn.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        HPSliderCtrl();
        //CD
        if (WorldControler.LimeyLV_9 == true)
            cdTimeSet = cdTimeSet_Basic - 6;
        else if (WorldControler.LimeyLV_6 == true)
            cdTimeSet = cdTimeSet_Basic - 3;
        else if (WorldControler.LimeyLV_3 == true)
            cdTimeSet = cdTimeSet_Basic - 1;
        else
            cdTimeSet = cdTimeSet_Basic;

        if (cdTimeCount <= cdTimeSet)
        {
            cdTimeCount += Time.deltaTime;
            reviveImg.fillAmount = cdTimeCount / cdTimeSet;
            m_Idle.GetComponent<TowerSetting>().hp = reviveImg.fillAmount * m_Idle.GetComponent<TowerSetting>().maxHp;
        }
        //爆炸範圍提示
        bool mouseOnBtn = RectTransformUtility.RectangleContainsScreenPoint(reviveImg.rectTransform, Input.mousePosition);
        if (mouseOnBtn == true)
        {
            range.SetActive(true);
        }
        if (mouseOnBtn == false)
        {
            range.SetActive(false);
        }
        //復活
        if (reviveImg.fillAmount >= 1)
        {
            ubBtn.gameObject.SetActive(false);
            gameObject.SetActive(false);
            m_Idle.SetActive(true);
            cdTimeCount = 0;
        }
        //奧義冷卻
        if (ubCdTimeCount <= ubCdTimeSet)
        {
            ubCdTimeCount += Time.deltaTime;
            ubBtn.interactable = false;
            ubBtn.image.fillAmount = ubCdTimeCount / ubCdTimeSet;
        }
        if (ubCdTimeCount >= ubCdTimeSet)
        {
            ubBtn.interactable = true;
        }

    }
    public void UbClick()
    {
        m_Idle.GetComponent<TowerSetting>().hp = m_Idle.GetComponent<TowerSetting>().maxHp;
        reviveImg.fillAmount = 1;
    }
    public void Attack()
    {
        attack.SetActive(true);
    }
    void HPSliderCtrl()
    {

        hpSlider.fillAmount = m_Idle.GetComponent<TowerSetting>().hp / m_Idle.GetComponent<TowerSetting>().maxHp;
        //pictureHpSlider.fillAmount = m_Idle.GetComponent<TowerSetting>().hp / m_Idle.GetComponent<TowerSetting>().maxHp;
    }


    //選取框
    private void OnMouseOver()
    {
        if (inShadow == false && WorldControler.buildType == 0 && !EventSystem.current.IsPointerOverGameObject())
        {
            selectFrame.SetActive(true);
        }
    }
    private void OnMouseExit()
    {
        if (inShadow == false && WorldControler.buildType == 0)
        {
            selectFrame.SetActive(false);
        }
    }
    private void OnMouseDown()
    {
        if (inShadow == false && WorldControler.buildType == 0)
        {
            GameObject[] UIs = GameObject.FindGameObjectsWithTag("UI");
            for (int i = 0; i < UIs.Length; i++)
            {
                UIs[i].SetActive(false);
            }

            TowerRange_UI.SetActive(true);
            towerPicture.SetActive(true);
            RemoveBtn.SetActive(true);
            selectedFrame.SetActive(true);
        }

    }
    public void RemoveTower()
    {
        WorldControler.crystal += tower_Depoly.GetComponent<TowerReadyDeploy>().usedCrystal * 0.2f;
        WorldControler.gas += tower_Depoly.GetComponent<TowerReadyDeploy>().usedGas * 0.2f;
        WorldControler.food += tower_Depoly.GetComponent<TowerReadyDeploy>().usedFood * 0.2f;

        WorldControler.population -= tower_Depoly.GetComponent<TowerReadyDeploy>().usedPopulation;

        GameObject i_Dialog = GameObject.Find("Teachs").GetComponent<FindSupport>().needToFind_1;
        i_Dialog.GetComponent<ImmediateDialog>().limey_Out = true;
        i_Dialog.GetComponent<ImmediateDialog>().eliteTalking = true;
        i_Dialog.SetActive(true);
        Destroy(transform.parent.gameObject);
    }
}
