
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UB_Note : MonoBehaviour
{
    [SerializeField]
    GameObject t_Note;
    // Update is called once per frame
    void Update()
    {
        bool mouseOnBtn = RectTransformUtility.RectangleContainsScreenPoint(gameObject.GetComponent<Button>().image.rectTransform, Input.mousePosition);
        if (mouseOnBtn == true)
        {
            t_Note.SetActive(true);
        }
        else
        {
            t_Note.SetActive(false);
        }
    }
}
