using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UB_XiaLei : MonoBehaviour
{
    [Header("而竡秙")]
    [SerializeField]
    Button ubBtn;

    [Header("而竡ю阑")]
    [SerializeField]
    GameObject ubAttack;
    [Header("而竡疭")]
    [SerializeField]
    GameObject s_ubAttack;
    [Header("而竡絛瞅")]
    [SerializeField]
    GameObject ubRange;

    [Header("而竡玱")]
    [SerializeField]
    float cdTimeSet;
    [SerializeField]
    float cdTimeCount;

    [SerializeField]
    Animator a_XiaLei;

    [SerializeField]
    GameObject music_UB;

    void Start()
    {
        
    }

    void Update()
    {
        //CD
        if(cdTimeCount <= cdTimeSet)
        {
            cdTimeCount += Time.deltaTime;
            ubBtn.interactable = false;
            ubBtn.image.fillAmount = cdTimeCount / cdTimeSet;
        }
        if(cdTimeCount >= cdTimeSet)
        {
            ubBtn.interactable = true;
        }

        //而竡絛瞅矗ボ
        bool mouseOnBtn = RectTransformUtility.RectangleContainsScreenPoint(ubBtn.image.rectTransform, Input.mousePosition);
        if (mouseOnBtn == true)
        {
            ubRange.SetActive(true);
        }
        if (mouseOnBtn == false)
        {
            ubRange.SetActive(false);
        }
    }
    public void UseUb()
    {
        cdTimeCount = 0;
        a_XiaLei.SetBool("UB", true);
        music_UB.SetActive(true);
    }
}
